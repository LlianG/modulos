export const environment = {
  production: true,

  firebase: {
  },

  debug: false,
  log: {
    auth: false,
    store: false,
  },

  smartadmin: {
    api: null,
    db: 'smartadmin-angular'
  },
  urlApp: 'http://mobilsoft.solutions',
  urlMaster: 'http://mobilsoft.solutions:3000',
  token_client: {
      key: 'kcNMuSQ5AWmACo6e',
      iv: 'eb5mTOf1ZsrUWRhV'
  }
};
