import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormControl } from '@angular/forms';
import { FacturacionClass, FacturacionTotalesClass, FacturacionCreditoClass, FacturacionArraysClass } from '../facturacion.class';
import { FacturacionService } from '../facturacion.service';
import { SnotifyService } from 'ng-snotify';
import { Router } from '@angular/router';
import { DxDataGridComponent } from 'devextreme-angular';

@Component({
    selector: 'app-facturacion-form',
    templateUrl: 'facturacion.form.component.html',
    styleUrls: ['facturacion.form.component.css']
})

export class FacturacionFormComponent implements OnInit {
    @ViewChild('gridTerceros') dataGrid: DxDataGridComponent;
    @ViewChild('gridProductos') dataGridProductos: DxDataGridComponent;
    
    getSelectedRowsData() {
        return this.dataGrid.instance.getSelectedRowsData()[0]
    }
    c_accion: number;
    tipFacRap: number;
    TipoFac: number;
    facturacion: FacturacionTotalesClass;
    cupo_credito: FacturacionCreditoClass;
    formData: FacturacionClass;
    dataForm: FacturacionArraysClass;
    valCorreo: boolean;
    terceros: any;
    products: any =[];
    ciudades:any;
    departamentos: any;
    listado_precios: any;
    tipval_fac: number;

    @Input() set accion(accion: number) {
        this.c_accion = accion;
        switch (accion) {
            case 1: //:: nuevo
                this.formData = new FacturacionClass();
                this.consecutivo();
                this.ngxSmartModalService.getModal('facturacion_form_modal').open();
                break;
            case 2: //:: editar
                this.ngxSmartModalService.getModal('facturacion_form_modal').open();
                break;
        }
    }
    @Input() set idIn(id: number) {
        if (id) { //:: editar
            this.formData = new FacturacionClass();
            // this.almacenService.listar_almacen(id).then(data => {
            //     this.formData = data;
            // })
        }
    }
    @Output() close = new EventEmitter();


    constructor(
        public ngxSmartModalService: NgxSmartModalService,
        private snotifyService: SnotifyService,
        private router:Router,
        private facturacionService: FacturacionService
    ) {
        this.formData = new FacturacionClass();
        this.facturacion = new FacturacionTotalesClass();
        this.cupo_credito = new FacturacionCreditoClass();
        this.cupo_credito = new FacturacionCreditoClass();
        this.dataForm = new FacturacionArraysClass();
        this.tipFacRap = 1;
        this.TipoFac = 0;
        this.valCorreo = true;
        this.tipval_fac = 2;
    }

    ngOnInit() { 
        this.listarTerceros();
        this.listarDepartamento();
        this.listarAlmacenes();
        this.listar_insumos();
        this.listaPrecios();
    }

    listaPrecios(){
        this.facturacionService.lista_precios({
            codalm: this.formData.ALMACEN,
            tiptar: 1,
            accion: 'L'
        }).then(resp=>{
            console.log(resp)
        })
    }

    listarTerceros(){
        this.facturacionService.listar_terceros({}).then(resp=>{
            this.terceros = resp.filas.recordset
        })
    }
    listarDepartamento(){
        this.facturacionService.listar_departamentos({}).then(resp=>{
            this.departamentos = resp.filas.recordsets[0];
            this.ciudades = resp.filas.recordsets[1]
            this.departamentos.forEach(element => {
                this.dataForm.ARRAY_DEPARTAMENTO.push({
                    ID: element.coddep,
                    NOMBRE: element.nomdep
                })
            });
            this.ciudades.forEach(element => {
                this.dataForm.ARRAY_CIUDAD.push({
                    ID: element.coddane,
                    NOMBRE: element.nommun
                })
            });
            
        })
    }
    listarAlmacenes(){
        this.facturacionService.listar_almacenes({
            estado: 1,
            accion: 'L'
        }).then(resp => {
            const almacenes = resp.filas.recordsets[0];
            almacenes.forEach(element => {
                this.dataForm.ARRAY_ALMACEN.push({
                    ID: element.codalm,
                    NOMBRE: element.nomalm
                })
            })
        })
    }
    listar_insumos(){
        this.facturacionService.listar_insumos({}).then(resp=>{
          this.dataGridProductos.dataSource = resp.filas.recordset;
        })
    }
    cerrar() {
        this.ngxSmartModalService.getModal('facturacion_form_modal').close();
        this.close.emit({ tipo: 1 });
    }

    async consecutivo(){
        //const consecutivo = await this.facturacionClass.buscar_consecutivo().then(result=>result);
        //this.formData.codigo = consecutivo.codigo;
    }

    async guardar(form: FormControl) {
        this.snotifyService.async('Guardando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const data = Object.assign({}, this.formData);
                    //await this.facturacionClass.guardar_almacen(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Almacen gurdado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                    this.ngxSmartModalService.getModal('facturacion_form_modal').close();
                    this.close.emit({ tipo: 2 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo guardar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    async editar(form: FormControl) {
        this.snotifyService.async('Actualizando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const data = Object.assign({}, this.formData);
                    //await this.almacenService.actualizar_almacen(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Almacen actualizado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                    this.ngxSmartModalService.getModal('facturacion_form_modal').close();
                    this.close.emit({ tipo: 3 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo actualizar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    atras(){
        this.router.navigate(['admin/facturacion'])
    }
    verificar_accion(data){

    }
    cancel(){
        
    }

    cambio_tipo_facturacion(event){
        this.TipoFac = (event.target.checked)?1:0
        this.tipFacRap = 1
        this.formData = new FacturacionClass()
    }

    agregar_concepto(){
        
    }
    recalcular(){
        this.calcular()
    }

    validarEmail(valor) {
        if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(valor)){
           this.valCorreo = true;
        } else {
           this.valCorreo = false;
        }
    }

    modal_usuarios(){
        this.ngxSmartModalService.getModal('myModal').open()
    }

    modal_productos(){
        this.ngxSmartModalService.getModal('productos').open()
    }

    selectionChangedHandler(event) {
        const data = event.data
        this.formData.IDENTIFICACION_CLIENTE = data.codter;
        this.formData.NOMBRES_CLIENTE = data.nomter;
        this.formData.DIRECCION = data.dirter;
        this.formData.TELEFONO = data.telter;
        this.formData.CELULAR = data.celter;
        this.formData.CIUDAD = data.coddane;
        this.formData.DEPARTAMENTO = (this.ciudades.filter(item => item.coddane == data.coddane).length > 0)?this.ciudades.filter(item => item.coddane == data.coddane)[0].coddep: null
        this.ngxSmartModalService.getModal('myModal').close()
        /* if (this.getSelectedRowsData()) {
            console.log(this.getSelectedRowsData());
        } */
    }

    selectionChangedHandler2(event) {
        const data = event.data
        if(this.products.filter(item => item.codins == data.codins).length > 0){
            this.snotifyService.warning('Este producto ya ha sido agregado a la lista.', 'Atención')
        }else{
            console.log(data)
            this.products.push(data)
        }
    }
    quitar(i){
        this.products.splice(i, 1)
        console.log(this.products)
        this.recalcular()
    }
    calcular_fecha(){
        const d = new Date()
        const nuevaFecha =  new Date(d.setDate(d.getDate() + (this.formData.PLAZO-1)));
        this.formData.FECHA_VENCIMIENTO = nuevaFecha.getDate()+'/'+nuevaFecha.getMonth()+'/'+nuevaFecha.getFullYear()
    }
    eliminarProducto(data){

    }
    calcular(){
        let total_desc = 0;
        let total_car = 0;
        let subtotal = 0;
        let total_iva = 0;
        let total = 0;
        let valor_1 = 0;
        let valor_2 = 0;
        let valor_retefuente = 0;
        let valor_reteica = 0;
        let valor_reteiva = 0;
        let fletes = this.facturacion.FLETES;
        let ajuste_peso = 0;
        for (var i = 0; i < this.products.length; i++) {
            var element = this.products[i];
            if (this.TipoFac) { //FACTURACION RAPIDA
                // if (element.canform > this.ver_mis_existencia(element.codins)) {
                //     element.canform = this.ver_mis_existencia(element.codins)
                //     $.notify({
                //         icon: 'fa fa-exclamation',
                //         title: "<strong>No hay mas inventario para este producto</strong>",
                //         message: ""
                //     }, {
                //         type: 'warning'
                //     });
                // }
                /* var spo = this.listado_precios.map( (producto)  => {
                    return producto.codins
                }).indexOf(element.codins)
                if (pos != -1) {
                    switch (this.facturacion.tippag) {
                        case 1:
                            element.valneg = this.listado_precios[spo].precio_full_ai
                            break;
                        case 2:
                            element.valneg = this.listado_precios[spo].precio_credito_20_ai
                            break;
                        default:
                            element.valneg = this.listado_precios[spo].precio_credito_30_ai
                            break;
                    }
                } */
            }

            if (element.desund > 0) {
                element.carund = 0;
                this.products[i].carund = 0;
            } else {
                element.desund = 0
                this.products[i].desund = 0;
            }

            if (this.products[i].carund == null) {
                element.carund = 0
                this.products[i].carund = 0;
            }


            var vali_carg = Math.round(element.carund / ((100 + 16) / 100));
            var vali_desc = Math.round(element.desund / ((100 + 16) / 100));
            var val = 0
            var valnet = 0
            if (this.tipval_fac == 1) {
                val = Math.round(element.PRECIO_LISTA)
                valnet = Math.round(val + vali_carg)
            } else {
                val = Math.round(element.PRECIO_LISTA)
                valnet = Math.round(val + vali_carg)
                total_desc += (vali_desc * element.canform)
            }

            total_car += (vali_carg * element.canform)

            subtotal += Math.round(element.canform * val)
            total_iva += Math.round((valnet * (16 / 100))) * element.canform
            total += (valnet + Math.round(valnet * (16 / 100))) * element.canform

            if (this.TipoFac) {
                this.products[i].valins_lista = (valnet + Math.round(valnet * (16 / 100)));
                this.products[i].valins_negociado = (valnet + Math.round(valnet * (16 / 100)));
            }
        }

        /* for (var i = 0; i < this.products.length; i++) {
            var producto = this.products[i];
            var pos = this.listado_precios.map(function (detalle) {
                return detalle.codins
            }).indexOf(producto.codins)
            if (pos != -1) {
                valor_1 = valor_1 + (this.listado_precios[pos].precio_full * producto.canform)
                valor_2 = valor_2 + (this.listado_precios[pos].precio_credito_20 * producto.canform)
            }
        } */
        if (this.tipFacRap == 2 && this.TipoFac == 1)
            total = total - valor_retefuente - valor_reteica - valor_reteiva + fletes + ajuste_peso
        else total = total - valor_retefuente - valor_reteica - valor_reteiva + fletes + ajuste_peso


        this.facturacion.SUBTOTAL = subtotal;
        this.facturacion.DESCUENTOS = total_desc;
        this.facturacion.CARGOS = total_car;
        this.facturacion.IVA = total_iva;
        this.facturacion.RETEFUENTE = valor_retefuente;
        this.facturacion.RETEICA = valor_reteica;
        this.facturacion.RETEIVA = valor_reteiva;
        this.facturacion.TOTAL = total;
    }
    seleccion_prefijo(){
        this.formData.CODIGO_ALMACEN = this.formData.ALMACEN.toString();
        this.formData.CONSECUTIVO = '000001'
    }
    ver_mis_existencia(i) {
        if(isNaN(parseInt(this.products[i].canform))){
            this.products[i].canform = 0;
            this.recalcular()
        }else{
            if(parseInt(this.products[i].caninv) > parseInt(this.products[i].canform)){
                this.recalcular()
            }else{
                this.products[i].canform = this.products[i].caninv
                this.recalcular()
            }
        }
        
    }

}