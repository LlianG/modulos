import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP } from '@app/constant';
// import { PdfMakeService, dataPdfMake, dataLogPdf } from '@app/core/pdfmake.config';
 
@Injectable({
    providedIn: 'root'
})
export class FacturacionService {
    apiEndpoint = APP.ApiEndpoint;
    AppBaseUrl = APP.AppBaseUrl;
    constructor(
        private http: HttpClient,
        // private pdfMakeService: PdfMakeService
    ) { 

    }
    async listar_terceros(data: any): Promise<any> {
        const url = `${this.apiEndpoint}/api/terceros/`;
        return await this.http.post<any>(url, data).toPromise();
    }
    async listar_departamentos(data: any){
        const url = `${this.apiEndpoint}/api/departamentos/`;
        return await this.http.post<any>(url, data).toPromise();
    }
    async listar_almacenes(data: any){
        const url = `${this.apiEndpoint}/api/almacenes/`;
        return await this.http.post<any>(url, data).toPromise();
    }
    async listar_insumos(data: any){
        const url = `${this.apiEndpoint}/api/insumos/`;
        return await this.http.post<any>(url, data).toPromise();
    }
    async lista_precios(data: any){
        const url = `${this.apiEndpoint}/api/listado_precios/`;
        return await this.http.post<any>(url, data).toPromise();
    }

    // async reportePDFFactura(data: any): Promise<any> {
    //     const that = this;
    //     return new Promise(async (resolve, reject) => {
    //       that.pdfMakeService.convertImgToBase64URL(that.AppBaseUrl + '/assets/img/pos/logo.png', async function (base64Img) {
    //         let data_impri: any = [
    //           [{
    //             text: 'CODIGO',
    //             style: 'tableHeader'
    //           }, {
    //             text: 'NOMBRES',
    //             style: 'tableHeader'
    //           },
    //           ]
    //         ];
    
    //         // const res = await that.listar_clases_filtrados(data).then(result => result);
    //         const res = [];
    //         for (let i = 0; i < res.length; i++) {
    //           const element = res[i];
    //           data_impri.push([
    //             {
    //               text: element.codigo,
    //               alignment: 'left'
    //             }, {
    //               text: element.nombre,
    //               alignment: 'left'
    //             }
    //           ]);
    //         }
    
    //         dataLogPdf.layout = dataPdfMake.line_layout;
    //         let docDefinition = {
    //           style: 'tableSubinfo',
    //           layout: dataPdfMake.line_layout,
    //           content: [
    //             {
    //               image: base64Img,
    //               width: 100
    //             },
    //             {
    //               text: 'LISTADO DE CLASES DE PRODUCTO',
    //               style: 'header'
    //             },
    //             {
    //               style: 'tableDetalle',
    //               layout: dataPdfMake.line_layout,
    //               fontSize: 5,
    //               table: {
    //                 headerRows: 1,
    //                 fontSize: 5,
    //                 widths: ['*', '*'],
    //                 body: data_impri
    //               }
    //             },
    //             dataLogPdf
    //           ],
    //           styles: dataPdfMake.styles,
    //           pageSize: 'LETTER',
    //           pageMargins: [20, 20, 20, 20]
    //         };
    //         resolve(docDefinition);
    //       }, null);
    //     });
    // }
}
