import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class NotasProveedorService {

    constructor(
        private http: HttpClient,
    ) { }
}
