export class RequisicionClass {
    constructor(
        public ID?: number,
        public NIT_EMPRESA?: string,
        public RAZON_SOCIAL?: string,
        public FECHA?: Date,
        public DIRECCION?: string,
        public ALMACEN?: number,
    ) {
        this.ID = null;
        this.NIT_EMPRESA = null;
        this.RAZON_SOCIAL = null;
        this.FECHA = new Date();
        this.DIRECCION = null;
        this.ALMACEN = null;
    }
}
export class RequisicionArraysClass {
    constructor(
        public ARRAY_ALMACEN?: Array<any>,
        public ARRAY_CUENTAS?: Array<any>,
    ) {
        this.ARRAY_ALMACEN = [{
            ID: null,
            NOMBRE: 'Seleccione un almacen'
        }]
        this.ARRAY_CUENTAS = [{
            ID: null,
            NOMBRE: 'Seleccione una cuenta'
        }]
    }
}