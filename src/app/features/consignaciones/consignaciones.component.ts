import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ConsignacionesService } from './consignaciones.service';
import { Router } from '@angular/router'

@Component({
    selector: 'app-consignaciones',
    templateUrl: './consignaciones.component.html',
    styleUrls: ['./consignaciones.component.css']
})
export class ConsignacionesComponent implements OnInit {

    c_id_table: number;
    c_accion_form: number;
    c_cambio_table: number;
    f_state_table: number;

    constructor(
        private consignacionesService: ConsignacionesService,
        private titleService: Title,
        private route: Router
    ) {
        this.titleService.setTitle('Consignaciones');
        this.c_accion_form = 0;
        this.c_cambio_table = 1;
        this.f_state_table = 1;
    }

    ngOnInit() {
    }

    newConsignaciones() {
        this.route.navigate(['admin/consignaciones/new'])
    }
    event_table_consignaciones(event: any): void {
        switch (event.tipo) {
            case 1: //:: Nuevo
                this.c_accion_form = event.tipo;
                break;
            case 2: //:: Editar
                this.c_accion_form = event.tipo;
                this.c_id_table = event.id;
                break;
            case 3: //:: Desactivado
                this.c_cambio_table++;
                break;
            case 4: //:: Reactivado
                this.c_cambio_table++;
                break;
        }
    }

    event_form_consignaciones(event): void {
        switch (event.tipo) {
            case 1: //:: Cerró

                break;
            case 2: //:: Guardó
                this.c_cambio_table++;
                break;
            case 3: //:: Editó
                this.c_cambio_table++;
                break;
        }
        this.c_accion_form = 0;
        this.c_id_table = null;
    }

}
