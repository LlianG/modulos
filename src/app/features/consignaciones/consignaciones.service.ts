import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ConsignacionesService {

    constructor(
        private http: HttpClient,
    ) { }
}
