export class NotasBancariasClass {
    constructor(
        public ID?: number,
        public NIT_EMPRESA?: string,
        public RAZON_SOCIAL?: string,
        public FECHA?: Date,
        public NUMERO_REFERENCIA?: string,
        public ALMACEN?: number,
        public DESCRIPCION?: string
    ) {
        this.ID = null;
        this.NIT_EMPRESA = null;
        this.RAZON_SOCIAL = null;
        this.FECHA = new Date();
        this.NUMERO_REFERENCIA = null;
        this.ALMACEN = null;
        this.DESCRIPCION = null;

    }
}
export class NotasBancariasArraysClass {
    constructor(
        public ARRAY_ALMACEN?: Array<any>,
        public ARRAY_CUENTAS?: Array<any>,
    ) {
        this.ARRAY_ALMACEN = [{
            ID: null,
            NOMBRE: 'Seleccione un almacen'
        }]
        this.ARRAY_CUENTAS = [{
            ID: null,
            NOMBRE: 'Seleccione una cuenta'
        }]
    }
}
export class ConsignacionesResumenClass {
    constructor(
        public TOTAL_EFECTIVO?: number,
        public TOTAL_CHEQUES?: number,
        public TOTAL_CONSIGNACION?: number
    ){
        this.TOTAL_EFECTIVO = 0;
        this.TOTAL_CHEQUES = 0;
        this.TOTAL_CONSIGNACION = 0;
    }
}