import { Routes, RouterModule } from '@angular/router';

import {ModuleWithProviders} from "@angular/core";

/************************************\
 * IMPORTAMOS LOS COMPONENTES PARA PODER
 * SER LLAMADOS POR MEDIO DEL ROUTING 
\************************************/
import { BaseComponent } from './base/base.component';
import { HomeComponent } from './home/home.component';

import { FacturacionComponent } from './facturacion/facturacion.component';
import { FacturacionFormComponent } from './facturacion/form/facturacion.form.component';

import { ReciboDeCajaComponent } from './recibo-caja/recibo-caja.component';
import { ReciboDeCajaFormComponent } from './recibo-caja/form/recibo-caja.form.component';

import { PedidosComponent } from './pedidos/pedidos.component';
import { PedidosFormComponent } from './pedidos/form/pedidos.form.component';

import { RequisicionComponent } from './requisicion/requisicion.component';
import { RequisicionFormComponent } from './requisicion/form/requisicion.form.component';

import { NotasBancariasComponent } from './notas-bancarias/notas-bancarias.component';
import { NotasBancariasFormComponent } from './notas-bancarias/form/notas-bancarias.form.component';

import { ConsignacionesComponent } from './consignaciones/consignaciones.component';
import { ConsignacionesFormComponent } from './consignaciones/form/consignaciones.form.component';

import { NotasProveedorComponent } from './notas-proveedor/notas-proveedor.component';
import { NotasProveedorFormComponent } from './notas-proveedor/form/notas-proveedor.form.component';

import { InventarioAlmacenComponent } from './inventario-almacen/inventario-almacen.component';
import { InventarioAlmacenFormComponent } from './inventario-almacen/form/inventario-almacen.form.component';

import { EgresosComponent } from './egresos/egresos.component';
import { EgresosFormComponent } from './egresos/form/egresos.form.component';

import { NotasClientesComponent } from './notas-clientes/notas-clientes.component';
import { NotasClientesFormComponent } from './notas-clientes/form/notas-clientes.form.component';

import { RadicacionFacturaComponent } from './radicacion-factura/radicacion-factura.component';
import { RadicacionFacturaFormComponent } from './radicacion-factura/form/radicacion-factura.form.component';

import { DevolucionesClientesComponent } from './devoluciones-clientes/devoluciones-clientes.component';
import { DevolucionesClientesFormComponent } from './devoluciones-clientes/form/devoluciones-clientes.form.component';

import { FacturacionMobilsoftComponent } from './facturacion-mobilsoft/facturacion-mobilsoft.component';
import { FacturacionMobilsoftFormComponent } from './facturacion-mobilsoft/form/facturacion-mobilsoft.form.component';

export const featureRoutes: Routes = [
    {
        path: 'home',
        component: HomeComponent,
        data: {pageTitle: 'Home'}
    },
    {
        path: 'base',
        component: BaseComponent,
        data: {pageTitle: 'Base'}
    },
    {
        path: 'facturacion',
        component: FacturacionComponent,
        data: {pageTitle: 'Facturacion'}
    },
    {
        path: 'facturacion/new',
        component: FacturacionFormComponent,
        data: {pageTitle: 'Facturacion/Nueva factura'}
    },
    {
        path: 'recibo-caja',
        component: ReciboDeCajaComponent,
        data: {pageTitle: 'Recibo de caja'}
    },
    {
        path: 'recibo-caja/new',
        component: ReciboDeCajaFormComponent,
        data: {pageTitle: 'Recibo de caja/Nuevo recibo de caja'}
    },
    {
        path: 'pedido',
        component: PedidosComponent,
        data: {pageTitle: 'Pedidos'}
    },
    {
        path: 'pedido/new',
        component: PedidosFormComponent,
        data: {pageTitle: 'Pedidos/Nuevo pedido'}
    },
    {
        path: 'requisicion',
        component: RequisicionComponent,
        data: {pageTitle: 'Requsiciones'}
    },
    {
        path: 'requisicion/new',
        component: RequisicionFormComponent,
        data: {pageTitle: 'Requisiciones/Nueva requisicion'}
    },
    {
        path: 'notas-bancarias',
        component: NotasBancariasComponent,
        data: {pageTitle: 'Notas bancarias'}
    },
    {
        path: 'notas-bancarias/new',
        component: NotasBancariasFormComponent,
        data: {pageTitle: 'Notas bancarias/Nueva nota bancaria'}
    },
    {
        path: 'consignaciones',
        component: ConsignacionesComponent,
        data: {pageTitle: 'Consignaciones'}
    },
    {
        path: 'consignaciones/new',
        component: ConsignacionesFormComponent,
        data: {pageTitle: 'Consignaciones/Nueva consignación'}
    },
    {
        path: 'nota-proveedor',
        component: NotasProveedorComponent,
        data: {pageTitle: 'Notas a proveedor'}
    },
    {
        path: 'nota-proveedor/new',
        component: NotasProveedorFormComponent,
        data: {pageTitle: 'Notas a proveedor/Nueva nota'}
    },
    {
        path: 'inventario-almacen',
        component: InventarioAlmacenComponent,
        data: {pageTitle: 'Inventario de almacen'}
    },
    {
        path: 'inventario-almacen/new',
        component: InventarioAlmacenFormComponent,
        data: {pageTitle: 'Inventario de almacen/Nueva entrada/salida de almacen'}
    },
    {
        path: 'egresos',
        component: EgresosComponent,
        data: {pageTitle: 'Egresos'}
    },
    {
        path: 'egresos/new',
        component: EgresosFormComponent,
        data: {pageTitle: 'Egresos/Nuevo egreso'}
    },
    {
        path: 'notas-clientes',
        component: NotasClientesComponent,
        data: {pageTitle: 'Notas a clientes'}
    },
    {
        path: 'notas-clientes/new',
        component: NotasClientesFormComponent,
        data: {pageTitle: 'Notas a clientes/Nueva nota'}
    },
    {
        path: 'radicacion-factura',
        component: RadicacionFacturaComponent,
        data: {pageTitle: 'Radicación de facturas'}
    },
    {
        path: 'radicacion-factura/new',
        component: RadicacionFacturaFormComponent,
        data: {pageTitle: 'Radicación de facturas/Nueva radicación'}
    },
    {
        path: 'devoluciones-clientes',
        component: DevolucionesClientesComponent,
        data: {pageTitle: 'Devoluciones a clientes'}
    },
    {
        path: 'devoluciones-clientes/new',
        component: DevolucionesClientesFormComponent,
        data: {pageTitle: 'Devoluciones a clientes/Nueva devolución'}
    },
    {
        path: 'facturacion-mobilsoft',
        component: FacturacionMobilsoftComponent,
        data: {pageTitle: 'Facturacion mobilsoft'}
    },
    {
        path: 'facturacion-mobilsoft/new',
        component: FacturacionMobilsoftFormComponent,
        data: {pageTitle: 'Facturacion mobilsoft/Nueva facturacion'}
    },

];

export const featureRouting:ModuleWithProviders = RouterModule.forChild(featureRoutes);

