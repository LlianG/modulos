import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { InventarioAlmacenService } from './inventario-almacen.service';
import { Router } from '@angular/router'

@Component({
    selector: 'app-inventario-almacen',
    templateUrl: './inventario-almacen.component.html',
    styleUrls: ['./inventario-almacen.component.css']
})
export class InventarioAlmacenComponent implements OnInit {

    c_id_table: number;
    c_accion_form: number;
    c_cambio_table: number;
    f_state_table: number;

    constructor(
        private inventarioAlmacenService: InventarioAlmacenService,
        private titleService: Title,
        private route: Router
    ) {
        this.titleService.setTitle('Inventario de almacen');
        this.c_accion_form = 0;
        this.c_cambio_table = 1;
        this.f_state_table = 1;
    }

    ngOnInit() {
    }

    newInventarioAlmacen() {
        this.route.navigate(['admin/inventario-almacen/new'])
    }
    event_table_inventario_almacen(event: any): void {
        switch (event.tipo) {
            case 1: //:: Nuevo
                this.c_accion_form = event.tipo;
                break;
            case 2: //:: Editar
                this.c_accion_form = event.tipo;
                this.c_id_table = event.id;
                break;
            case 3: //:: Desactivado
                this.c_cambio_table++;
                break;
            case 4: //:: Reactivado
                this.c_cambio_table++;
                break;
        }
    }

    event_form_inventario_almacen(event): void {
        switch (event.tipo) {
            case 1: //:: Cerró

                break;
            case 2: //:: Guardó
                this.c_cambio_table++;
                break;
            case 3: //:: Editó
                this.c_cambio_table++;
                break;
        }
        this.c_accion_form = 0;
        this.c_id_table = null;
    }

}
