import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormControl } from '@angular/forms';
import { InventarioAlmacenClass, InventarioAlmacenArraysClass, InventarioAlmacenMovimientoClass } from '../inventario-almacen.class';
import { InventarioAlmacenService } from '../inventario-almacen.service';
import { SnotifyService } from 'ng-snotify';
import { Router } from '@angular/router'

@Component({
    selector: 'app-inventario-almacen-form',
    templateUrl: 'inventario-almacen.form.component.html',
    styleUrls: ['inventario-almacen.form.component.css']
})

export class InventarioAlmacenFormComponent implements OnInit {
    c_accion: number;
    
    tipFacRap: number;
    formData: InventarioAlmacenClass;
    dataForm: InventarioAlmacenArraysClass;
    totalTonelaje: number;
    movimiento: InventarioAlmacenMovimientoClass;
    @Input() set accion(accion: number) {
        this.c_accion = accion;
        switch (accion) {
            case 1: //:: nuevo
                this.formData = new InventarioAlmacenClass();
                this.consecutivo();
                break;
            case 2: //:: editar
                // this.ngxSmartModalService.getModal('facturacion_form_modal').open();
                break;
        }
    }
    @Input() set idIn(id: number) {
        if (id) { //:: editar
            this.formData = new InventarioAlmacenClass();
            // this.almacenService.listar_almacen(id).then(data => {
            //     this.formData = data;
            // })
        }
    }
    @Output() close = new EventEmitter();


    constructor(
        public ngxSmartModalService: NgxSmartModalService,
        private snotifyService: SnotifyService,
        private router:Router
    ) {
        this.formData = new InventarioAlmacenClass();
        this.dataForm = new InventarioAlmacenArraysClass();
        this.movimiento = new InventarioAlmacenMovimientoClass();
        this.tipFacRap = 1;
        this.totalTonelaje = 0;
    }

    ngOnInit() { }

    cerrar() {
        this.ngxSmartModalService.getModal('facturacion_form_modal').close();
        this.close.emit({ tipo: 1 });
    }

    async consecutivo(){
        //const consecutivo = await this.InventarioAlmacenClass.buscar_consecutivo().then(result=>result);
        //this.formData.codigo = consecutivo.codigo;
    }

    async guardar(form: FormControl) {
        this.snotifyService.async('Guardando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const data = Object.assign({}, this.formData);
                    //await this.InventarioAlmacenClass.guardar_almacen(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Almacen gurdado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                    this.ngxSmartModalService.getModal('facturacion_form_modal').close();
                    this.close.emit({ tipo: 2 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo guardar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    async editar(form: FormControl) {
        this.snotifyService.async('Actualizando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const data = Object.assign({}, this.formData);
                    //await this.almacenService.actualizar_almacen(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Almacen actualizado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                    this.ngxSmartModalService.getModal('facturacion_form_modal').close();
                    this.close.emit({ tipo: 3 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo actualizar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    atras(){
        this.router.navigate(['admin/requisicion'])
    }
    verificar_accion(data){

    }
    cancel(){
        
    }
}