export class InventarioAlmacenClass {
    constructor(
        public ID?: number,
        public NIT_EMPRESA?: string,
        public RAZON_SOCIAL?: string,
        public FECHA?: Date,
        public DIRECCION?: string,
        public ALMACEN_ENTRADA?: number,
        public ALMACEN_SALIDA?: number,
        public TIPO?: number,
        public CONCEPTO_SALIDA?: number,
        public CONCEPTO_ENTRADA?: number,
    ) {
        this.ID = null;
        this.NIT_EMPRESA = null;
        this.RAZON_SOCIAL = null;
        this.FECHA = new Date();
        this.DIRECCION = null;
        this.ALMACEN_SALIDA = null;
        this.ALMACEN_ENTRADA = null;
        this.TIPO = 1;
        this.CONCEPTO_ENTRADA = null;
        this.CONCEPTO_SALIDA = null;
    }
}
export class InventarioAlmacenArraysClass {
    constructor(
        public ARRAY_ALMACEN?: Array<any>,
        public ARRAY_CUENTAS?: Array<any>,
        public ARRAY_TIPO?: Array<any>,
        public ARRAY_CONCEPTO?: Array<any>
    ) {
        this.ARRAY_ALMACEN = [{
            ID: null,
            NOMBRE: 'Seleccione un almacen'
        }]
        this.ARRAY_CUENTAS = [{
            ID: null,
            NOMBRE: 'Seleccione una cuenta'
        }]
        this.ARRAY_CONCEPTO = [{
            ID: null,
            NOMBRE: 'Seleccione un concepto'
        }]
        this.ARRAY_TIPO = [
        {
            ID: 1,
            NOMBRE: 'Entrada'
        },
        {
            ID: 2,
            NOMBRE: 'Salida'
        },{
            ID: 3,
            NOMBRE: 'Traslado'
        }]
    }
}

export class InventarioAlmacenMovimientoClass{
    constructor(
        public SUBTOTAL?: number,
        public TOTAL_IVA?: number,
        public TOTAL?: number
    ){
        this.SUBTOTAL = 0;
        this.TOTAL_IVA = 0;
        this.TOTAL = 0;
    }
}