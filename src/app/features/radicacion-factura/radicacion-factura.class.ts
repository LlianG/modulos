export class RadicacionFacturaClass {
    constructor(
        public ID?: number,
        public NIT_EMPRESA?: string,
        public RAZON_SOCIAL?: string,
        public FECHA?: Date,
        public NUMERO_REFERENCIA?: string,
        public ALMACEN?: number,
        public DESCRIPCION?: string,
        public DOCUMENTO?: string,
        public CONCEPTO?: string,
        public TELEFONO_PROVEEDOR?: string,
        public FECHA_COMPRA?: Date,
        public DIAS_VENCE?: number,
        public FECHA_VENCE?: Date,
        public NUMERO_FACTURA_PROVEEDOR?: string,
        public DIRECCION_PROVEEDOR?: string
    ) {
        this.ID = null;
        this.NIT_EMPRESA = null;
        this.RAZON_SOCIAL = null;
        this.FECHA = new Date();
        this.NUMERO_REFERENCIA = null;
        this.ALMACEN = null;
        this.DESCRIPCION = null;
        this.DOCUMENTO = null;
        this.CONCEPTO = null;
        this.TELEFONO_PROVEEDOR = null;
        this.FECHA_COMPRA = new Date();
        this.DIAS_VENCE = 0;
        this.FECHA_VENCE = new Date();
        this.NUMERO_FACTURA_PROVEEDOR = null;
        this.DIRECCION_PROVEEDOR = null;
    }
}
export class RadicacionFacturaArraysClass {
    constructor(
        public ARRAY_ALMACEN?: Array<any>,
        public ARRAY_CUENTAS?: Array<any>,
    ) {
        this.ARRAY_ALMACEN = [{
            ID: null,
            NOMBRE: 'Seleccione un almacen'
        }]
        this.ARRAY_CUENTAS = [{
            ID: null,
            NOMBRE: 'Seleccione una cuenta'
        }]
    }
}
export class RadicacionFacturaResumenClass {
    constructor(
        public SUBTOTAL?: number,
        public TOTA_DESCUENTO?: number,
        public TOTAL_IVA?: number,
        public TOTAL_RETEFUENTE?: number,
        public TOTAL_RETEICA?: number,
        public TOTAL_COMPRA?: number,
        public FLETES?: number,
        public SEGUROS?: number
    ){
        this.SUBTOTAL = 0;
        this.TOTA_DESCUENTO = 0;
        this.TOTAL_IVA = 0;
        this.TOTAL_RETEFUENTE = 0;
        this.TOTAL_RETEICA = 0;
        this.TOTAL_COMPRA = 0;
        this.FLETES = 0;
        this.SEGUROS = 0;
    }
}

export class RadicacionFacturaNotasClientesClass{
    constructor(
        public ABONOS?: number,
        public ABONOS_IVA?: number,
        public SALDO_NUEVO?: number,
    ){
        this.ABONOS = 0;
        this.ABONOS_IVA = 0;
        this.SALDO_NUEVO = 0;
    }
}