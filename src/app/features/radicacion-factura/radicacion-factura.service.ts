import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class RadicacionFacturaService {

    constructor(
        private http: HttpClient,
    ) { }
}
