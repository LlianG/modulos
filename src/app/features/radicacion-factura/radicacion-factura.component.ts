import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { RadicacionFacturaService } from './radicacion-factura.service';
import { Router } from '@angular/router'

@Component({
    selector: 'app-radicacion-factura',
    templateUrl: './radicacion-factura.component.html',
    styleUrls: ['./radicacion-factura.component.css']
})
export class RadicacionFacturaComponent implements OnInit {

    c_id_table: number;
    c_accion_form: number;
    c_cambio_table: number;
    f_state_table: number;

    constructor(
        private radicacionFacturaService: RadicacionFacturaService,
        private titleService: Title,
        private route: Router
    ) {
        this.titleService.setTitle('Notas a clientes');
        this.c_accion_form = 0;
        this.c_cambio_table = 1;
        this.f_state_table = 1;
    }

    ngOnInit() {
    }

    newRadicacion() {
        this.route.navigate(['admin/radicacion-factura/new'])
    }
    event_table_radicacion_factura(event: any): void {
        switch (event.tipo) {
            case 1: //:: Nuevo
                this.c_accion_form = event.tipo;
                break;
            case 2: //:: Editar
                this.c_accion_form = event.tipo;
                this.c_id_table = event.id;
                break;
            case 3: //:: Desactivado
                this.c_cambio_table++;
                break;
            case 4: //:: Reactivado
                this.c_cambio_table++;
                break;
        }
    }

    event_form_radicacion_factura(event): void {
        switch (event.tipo) {
            case 1: //:: Cerró

                break;
            case 2: //:: Guardó
                this.c_cambio_table++;
                break;
            case 3: //:: Editó
                this.c_cambio_table++;
                break;
        }
        this.c_accion_form = 0;
        this.c_id_table = null;
    }

}
