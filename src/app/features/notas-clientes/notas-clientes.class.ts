export class NotasClientesClass {
    constructor(
        public ID?: number,
        public NIT_EMPRESA?: string,
        public RAZON_SOCIAL?: string,
        public FECHA?: Date,
        public NUMERO_REFERENCIA?: string,
        public ALMACEN?: number,
        public DESCRIPCION?: string,
        public DOCUMENTO?: string,
        public CONCEPTO?: string,
        public TELEFONO_PROVEEDOR?: string,
    ) {
        this.ID = null;
        this.NIT_EMPRESA = null;
        this.RAZON_SOCIAL = null;
        this.FECHA = new Date();
        this.NUMERO_REFERENCIA = null;
        this.ALMACEN = null;
        this.DESCRIPCION = null;
        this.DOCUMENTO = null;
        this.CONCEPTO = null;
        this.TELEFONO_PROVEEDOR = null;

    }
}
export class NotasClientesArraysClass {
    constructor(
        public ARRAY_ALMACEN?: Array<any>,
        public ARRAY_CUENTAS?: Array<any>,
    ) {
        this.ARRAY_ALMACEN = [{
            ID: null,
            NOMBRE: 'Seleccione un almacen'
        }]
        this.ARRAY_CUENTAS = [{
            ID: null,
            NOMBRE: 'Seleccione una cuenta'
        }]
    }
}
export class NotasClientesResumenClass {
    constructor(
        public CUECON_BRUTO?: string,
        public NOMCUE_BRUTO?: string,
        public CUECON_TOTAL?: string,
        public NOMCUE_TOTAL?: string,
        public ABONOS?: number,
        public ABONOS_IVA?: number,
        public SALDO_NUEVO?: number,
    ){
        this.CUECON_BRUTO = null;
        this.NOMCUE_BRUTO = null;
        this.CUECON_TOTAL = null;
        this.NOMCUE_TOTAL = null;
        this.ABONOS = 0;
        this.ABONOS_IVA = 0;
        this.SALDO_NUEVO = 0;   

    }
}