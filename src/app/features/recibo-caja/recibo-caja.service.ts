import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP } from '@app/constant';
import { PdfMakeService, dataPdfMake, dataLogPdf } from '@app/core/pdfmake.config';
import { CurrencyPipe } from '@angular/common';

const line_layout = {
    hLineWidth: function (i, node) {
        return (i === 0 || i === node.table.body.length) ? 0.5 : 0.5;
    },
    vLineWidth: function (i, node) {
        return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
    },
    hLineColor: function (i, node) {
        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
    },
    vLineColor: function (i, node) {
        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
    },
}
@Injectable({
    providedIn: 'root'
})
export class ReciboDeCajaService {
    apiEndpoint = APP.ApiEndpoint;
    AppBaseUrl = APP.AppBaseUrl;
    
    constructor(
        private http: HttpClient,
        private pdfMakeService: PdfMakeService,
        private currencyPipe: CurrencyPipe
    ) { }

    async listar_empresa(): Promise<any> {
        const url = `${this.apiEndpoint}/api/empresa/listar`;
        return await this.http.get<any>(url).toPromise();
    }

    async listar_almacenes(): Promise<any>{
        const url = `${this.apiEndpoint}/api/almacenes/listar`;
        return await this.http.get<any>(url).toPromise();
    }

    async listar_cuentas(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/cuentas/listar`;
        return await this.http.post<any>(url, data).toPromise();
    }

    async listar_terceros(data: any): Promise<any> {
        const url = `${this.apiEndpoint}/api/terceros/listar`;
        return await this.http.get<any>(url).toPromise();
    }

    async listar_vendedores(): Promise<any> {
        const url = `${this.apiEndpoint}/api/vendedores/listar`;
        return await this.http.get<any>(url).toPromise();
    }

    async listar_facturas_pendientes(data: any): Promise<any> {
        const url = `${this.apiEndpoint}/api/recibo_caja/facturas_pendientes`;
        return await this.http.post<any>(url, data).toPromise();
    }

    async listar_formas_pago(): Promise<any>{
        const url = `${this.apiEndpoint}/api/facturacion-mobilsoft/listar_formas_pago`;
        return await this.http.get<any>(url).toPromise();
    }

    async listar_franquicias(): Promise<any>{
        const url = `${this.apiEndpoint}/api/recibo_caja/listar_franquicias`;
        return await this.http.get<any>(url).toPromise();
    }
    async listar_conceptos(): Promise<any>{
        const url = `${this.apiEndpoint}/api/concepto/listar`;
        return await this.http.get<any>(url).toPromise();
    }
    async reportePDFFactura(datos: any): Promise<any> {
        const that = this;
        return new Promise(async (resolve, reject) => {
            that.pdfMakeService.convertImgToBase64URL('./assets/img/pos/logo.png', async function (base64Img) {
                            let formData;
                            let servicios = [];
                            // let data_impri: any = [
                            //     [ 
                            //         {
                            //             text: 'Producto',
                            //             style: 'itemsHeader'
                            //         }, 
                            //         {
                            //             text: 'Valor',
                            //             style: [ 'itemsHeader', 'center']
                            //         }, 
                            //         {
                            //             text: 'IVA',
                            //             style: [ 'itemsHeader', 'center']
                            //         }, 
                            //         {
                            //             text: 'Desc.',
                            //             style: [ 'itemsHeader', 'center']
                            //         }, 
                            //         {
                            //             text: 'Total',
                            //             style: [ 'itemsHeader', 'center']
                            //         } 
                            //     ]
                            // ];
                            let data_impri: any = [
                                [
                                    {
                                        text: 'TIPO',
                                        style: 'tableHeader'
                                    }
                                    ,{
                                        text: 'FACTURA No.',
                                        style: 'tableHeader'
                                    },
                                    {
                                        text: 'VLR FACTURA',
                                        style: 'tableHeader'
                                    },
                                    {
                                        text: 'ABONO',
                                        style: 'tableHeader'
                                    }
                                ]
                                // [
                                //     {
                                //         text: 'CR',
                                //         style: 'tableHeader'
                                //     }
                                //     ,{
                                //         text: '0000024',
                                //         style: 'tableHeader'
                                //     },
                                //     {
                                //         text: '$1.200.000',
                                //         style: 'tableHeader'
                                //     },
                                //     {
                                //         text: '$1.200.000',
                                //         style: 'tableHeader'
                                //     }
                                // ]
                            ]
                            for (let i = 0; i < datos.facturas.length; i++) {
                                const element = datos.facturas[i];
                                data_impri.push([
                                    {
                                        text: element.modpag,
                                        style: 'tableHeader'
                                    }
                                    ,{
                                        text: element.comprobante,
                                        style: 'tableHeader'
                                    },
                                    {
                                        text:  that.currencyPipe.transform(element.total, 'USD', true, '1.0-0'),
                                        style: 'tableHeader'
                                    },
                                    {
                                        text: that.currencyPipe.transform(element.abono, 'USD', true, '1.0-0'),
                                        style: 'tableHeader'
                                    }
                                ])
                            }
                            

                            // const res = await that.listar_detalle_factura(datos).then(result => result);
                            const res = {filas:{recordsets:[]}}
                            // for (let i = 0; i < data.dataTable.length; i++) {
                            //     const element = data.dataTable[i];
                                
                            // }
                    
                            dataLogPdf.layout = line_layout;
                            let dd = {
                                style: 'tableSubinfo',
                                layout: line_layout,
                                content: [
                                    {
                                        image: base64Img,
                                        width: 150
                                    },
                                    {
                                        // izquierda, arriba, derecha, abajo
                                        text: 'No. '+datos.formData.NUMERO_COMPROBANTE ,
                                        style: 'header',
                                        margin: [390, -27, 20, 10]
                                    },
                                    {
                                        style: 'tableInfo',
                                        margin: [0, 5, 0, 0],
                                        layout: line_layout,
                                        table: {
                                            widths: ['*', '*', '*', '*'],
                                            body: [
                                                [
                                                    {
                                                        text: 'INFORMACION GENERAL',
                                                        alignment: 'center',
                                                        colSpan: 4,
                                                        style: 'tableHeader'
                                                    },
                                                    {},
                                                    {},
                                                    {}
                                                ],
                                                [
                                                    {
                                                        text: 'EMPRESA',
                                                        alignment: 'left',
                                                        style: 'tableHeader',
                                                        border: [true, true, false, false]
                                                    },
                                                    {
                                                        text: 'MOBILSOFT SAAS',
                                                        alignment: 'left',
                                                        style: 'tableSubinfo',
                                                        border: [false, true, true, false]
                                                    },
                                                    {
                                                        text: 'NIT:',
                                                        style: 'tableHeader',
                                                        alignment: 'left',
                                                        border: [false, true, false, false]
                                                    },
                                                    {
                                                        text: '123488551-9',
                                                        alignment: 'left',
                                                        style: 'tableSubinfo',
                                                        border: [false, true, true, false]
                                                    }
                                                ],
                                                [
                                                    {
                                                        text: 'CEDULA / NIT:',
                                                        alignment: 'left',
                                                        style: 'tableHeader',
                                                        border: [true, false, false, false]
                                                    },
                                                    {
                                                        text: datos.formData.IDENTIFICACION_CLIENTE,
                                                        alignment: 'left',
                                                        style: 'tableSubinfo',
                                                        border: [false, false, true, false]
                                                    },
                                                    {
                                                        text: 'NOMBRE DEL CLIENTE:',
                                                        style: 'tableHeader',
                                                        alignment: 'left',
                                                        border: [false, false, false, false]
                                                    },
                                                    {
                                                        text: datos.formData.NOMBRES_CLIENTE,
                                                        alignment: 'left',
                                                        style: 'tableSubinfo',
                                                        border: [false, false, true, false]
                                                    }
                                                ],
                                                [
                                                    {
                                                        text: 'IDENTIFICACION DEL COBRADOR:',
                                                        alignment: 'left',
                                                        style: 'tableHeader',
                                                        border: [true, false, false, false]
                                                    },
                                                    {
                                                        text: datos.formData.COBRADOR,
                                                        alignment: 'left',
                                                        style: 'tableSubinfo',
                                                        border: [false, false, true, false]
                                                    },
                                                    {
                                                        text: 'NOMBRE DEL COBRADOR:',
                                                        style: 'tableHeader',
                                                        alignment:  'left',
                                                        border: [false, false, false, false]
                                                    },
                                                    {
                                                        text: datos.formData.NOMBRE_COBRADOR,
                                                        alignment: 'left',
                                                        style: 'tableSubinfo',
                                                        border: [false, false, true, false]
                                                    }
                                                ],
                                                [
                                                    {
                                                        text: 'TOTAL A PAGAR:',
                                                        alignment: 'left',
                                                        style: 'tableHeader',
                                                        border: [true, false, false, true]
                                                    },
                                                    {
                                                        text: that.currencyPipe.transform(datos.recibocaja.TOTAL_PAGAR, 'USD', true, '1.0-0') ,
                                                        alignment: 'left',
                                                        style: 'tableSubinfo',
                                                        border: [false, false, true, true]
                                                    },
                                                    {
                                                        text: 'FECHA:',
                                                        style: 'tableHeader',
                                                        alignment: 'left',
                                                        border: [true, false, false, true]
                                                    },
                                                    {
                                                        text: new Date().getDate()+'/'+(new Date().getMonth()+1)+'/'+new Date().getFullYear(),
                                                        alignment: 'left',
                                                        style: 'tableSubinfo',
                                                        border: [false, false, true, true]
                                                    }
                                                ]
                                                
                                            ]
                                        }
                                    },
                                    // {
                                    //     style: 'tableInfo',
                                    //     layout: line_layout,
                                    //     table: {
                                    //         widths: [95, '*', 45, 150],
                                    //         layout: line_layout,
                                    //         body: [
                                    //             [
                                    //                 {
                                    //                     text: 'TOTAL A PAGAR:',
                                    //                     alignment: 'left',
                                    //                     style: 'tableHeader',
                                    //                     border: [true, false, false, true]
                                    //                 },
                                    //                 {
                                    //                     text:'$ 1.200.000',
                                    //                     alignment: 'left',
                                    //                     style: 'tableSubinfo',
                                    //                     border: [false, false, true, true]
                                    //                 },
                                    //                 {
                                    //                     text: 'FORMA DE PAGO:',
                                    //                     style: 'tableHeader',
                                    //                     alignment: 'left',
                                    //                     border: [false, false, false, true]
                                    //                 },
                                    //                 {
                                    //                     text: 'EF',
                                    //                     alignment: 'left',
                                    //                     style: 'tableSubinfo',
                                    //                     border: [false, false, true, true]
                                    //                 }
                                    //             ]
                                    //         ]
                                    //     }
                                    // },
                                        {
                                            style: 'tableDetalle',
                                            layout: line_layout,
                                            fontSize: 7,
                                            table: {
                                                headerRows: 1,
                                                fontSize: 7,
                                                widths: [80, '*', '*', '*'],
                                                body: data_impri
                                            }
                                        },
                                    {
                                        text: "\n"
                                    },
                                    {
                                        columns: [
                                            {
                                                style: 'tableInfo',
                                                margin: [0, 5, 0, 0],
                                                layout: line_layout,
                                                width: 150,
                                                table: {
                                                    widths: ['*'],
                                                    body: [
                                                        // izquierda, arriba, derecha, abajo
                                                        [
                                                            {
                                                                text: 'RESUMEN',
                                                                alignment: 'center',
                                                                style: 'tableHeader',
                                                                border: [true, true, true, true]
                                                            },
                                                        ],
                                                        [
                                                            {
                                                                text: 'ABONO: '+that.currencyPipe.transform(datos.recibocaja.FORMAS_PAGO, 'USD', true, '1.0-0'),
                                                                alignment: 'left',
                                                                style: 'tableHeader',
                                                                border: [true, false, true, false]
                                                            },
                                                        ],
                                                        [
                                                            {
                                                                text: 'DESCUENTO:'+that.currencyPipe.transform(datos.recibocaja.OTROS_DESCUENTOS, 'USD', true, '1.0-0'),
                                                                alignment: 'left',
                                                                style: 'tableHeader',
                                                                border: [true, false, true, false]
                                                            },
                                                        ],
                                                        [
                                                            {
                                                                text: 'RETENCIONES: '+that.currencyPipe.transform(datos.recibocaja.OTRAS_RETENCIONES, 'USD', true, '1.0-0'),
                                                                alignment: 'left',
                                                                style: 'tableHeader',
                                                                border: [true, false, true, false]
                                                            },
                                                        ],
                                                        [
                                                            {
                                                                text: 'OTROS CONCEPTOS: '+that.currencyPipe.transform(datos.recibocaja.OTROS_CARGOS, 'USD', true, '1.0-0'),
                                                                alignment: 'left',
                                                                style: 'tableHeader',
                                                                border: [true, false, true, true]
                                                            },
                                                        ],
                                                        [
                                                            {
                                                                text: 'Total: '+that.currencyPipe.transform(datos.recibocaja.TOTAL_PAGAR, 'USD', true, '1.0-0'),
                                                                alignment: 'left',
                                                                style: 'tableHeader',
                                                                border: [true, false, true, true]
                                                            },
                                                        ]
                                                    ]
                                                }
                                            },
                                            {},
                                            {
                                                style: 'tableInfo',
                                                margin: [0, 5, 0, 0],
                                                layout: line_layout,
                                                width: 210,
                                                table: {
                                                    widths: [65, '*'],
                                                    body: [
                                                        [
                                                            {
                                                                text: 'Elaborado Por:',
                                                                alignment: 'left',
                                                                style: 'tableHeader',
                                                                border: [true, true, true, true]
                                                            }, {
                                                                text: '',
                                                                alignment: 'left',
                                                                style: 'tableSubinfo',
                                                                border: [true, true, true, true]
                                                            }
                                                        ],
                                                        [
                                                            {
                                                                text: 'Reimpreso Por:',
                                                                style: 'tableHeader',
                                                                alignment: 'left',
                                                                border: [true, true, true, true]
                                                            }, {
                                                                text: '',
                                                                alignment: 'left',
                                                                style: 'tableSubinfo',
                                                                border: [true, true, true, true]
                                                            }
                                                        ]
                                                    ]
                                                }
                                            }
                                        ]
                                    }
                                ],
                                styles: dataPdfMake.styles,
                                pageSize: {
                                    width: 595.28,
                                    height: 370
                                },
                                pageMargins: [30, 30, 30, 30]
                            };
                            resolve(dd);
            }, null)
        });
    }
}
