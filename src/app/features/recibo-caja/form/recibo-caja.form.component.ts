import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormControl } from '@angular/forms';
import { ReciboDeCajaClass, ReciboDeCajaTotalesClass, ReciboDeCajaFormasClass, FacturacionArraysClass } from '../recibo-caja.class';
import { ReciboDeCajaService } from '../recibo-caja.service';
import { SnotifyService } from 'ng-snotify';
import { Router } from '@angular/router';
import { DxDataGridComponent } from 'devextreme-angular';
import { FacturacionMobilsoftService } from '@app/features/facturacion-mobilsoft/facturacion-mobilsoft.service';
import { openPdfMake } from '@app/core/pdfmake.config';
import { faEquals,faPlus, faHandScissors, faPercent, faMoneyBillWave, faMinus, faCreditCard } from '@fortawesome/free-solid-svg-icons';
import * as moment from 'moment';

@Component({
    selector: 'app-recibo-caja-form',
    templateUrl: 'recibo-caja.form.component.html',
    styleUrls: ['recibo-caja.form.component.css']
})

export class ReciboDeCajaFormComponent implements OnInit {
    @ViewChild('gridTerceros') dataGrid: DxDataGridComponent;
    @ViewChild('gridVendedores') dataGridVendedores: DxDataGridComponent;
    @ViewChild('gridConceptos') dataGridConceptos: DxDataGridComponent;

    getSelectedRowsData() {
        return this.dataGrid.instance.getSelectedRowsData()[0]
    }
    getSelectedRowsDataVendedores() {
        return this.dataGridVendedores.instance.getSelectedRowsData()[0]
    }
    getSelectedRowsDataConceptos() {
        return this.dataGridConceptos.instance.getSelectedRowsData()[0]
    }

    c_accion: number;
    
    tipFacRap: number;
    TipoFac: any;
    recibocaja: ReciboDeCajaTotalesClass;
    formData: ReciboDeCajaClass;
    dataForm: FacturacionArraysClass;
    terceros: any;
    vendedores: any;
    franquicias: any;
    facturas: any;
    conceptos: any;
    list_conceptos: any;
    forma_pago: ReciboDeCajaFormasClass;
    formas_pago_rc: any;
    icons = {faequals:faEquals, faplus:faPlus, faHandscissors: faHandScissors, faPercent:faPercent, faMoneyBillWave:faMoneyBillWave, faMinus:faMinus, faCreditCard: faCreditCard}
    icon1style = {
        'font-size': '17px',
        'position': 'absolute',
        'top': '18px',
        'left': '165px',
        'color': '#42c18787'
    }


    @Input() set accion(accion: number) {
        this.c_accion = accion;
        switch (accion) {
            case 1: //:: nuevo
                this.formData = new ReciboDeCajaClass();
                this.consecutivo();
                this.ngxSmartModalService.getModal('facturacion_form_modal').open();
                break;
            case 2: //:: editar
                this.ngxSmartModalService.getModal('facturacion_form_modal').open();
                break;
        }
    }
    @Input() set idIn(id: number) {
        if (id) { //:: editar
            this.formData = new ReciboDeCajaClass();
            // this.almacenService.listar_almacen(id).then(data => {
            //     this.formData = data;
            // })
        }
    }
    @Output() close = new EventEmitter();


    constructor(
        public ngxSmartModalService: NgxSmartModalService,
        private snotifyService: SnotifyService,
        private router:Router,
        private reciboDeCajaService: ReciboDeCajaService,
        private facturacionMobilsoftService: FacturacionMobilsoftService
    ) {
        this.formData = new ReciboDeCajaClass();
        this.recibocaja = new ReciboDeCajaTotalesClass();
        this.dataForm = new FacturacionArraysClass();
        this.forma_pago = new ReciboDeCajaFormasClass();
        this.tipFacRap = 1;
        this.list_conceptos = [];
        this.formas_pago_rc = []

    }

    ngOnInit() {
        this.listarEmpresa();
        this.listarAlmacenes();
        this.listarCuentas();
        this.listarTerceros();
        this.listarVendedores();
        this.listarFormas();
        this.listarFranquicia();
        this.listarConceptos();
    }

    cerrar() {
        this.ngxSmartModalService.getModal('facturacion_form_modal').close();
        this.close.emit({ tipo: 1 });
    }
    listarEmpresa(){
        this.reciboDeCajaService.listar_empresa().then(res => {
            this.formData.NIT_EMPRESA = res.filas.recordset[0].nitemp
            this.formData.RAZON_SOCIAL = res.filas.recordset[0].razemp
        })
    }
    listarAlmacenes(){
        this.reciboDeCajaService.listar_almacenes().then(res => {
            const alamacen = res.filas.recordset
            alamacen.forEach(element => {
                this.dataForm.ARRAY_ALMACEN.push({
                    ID: element.id,
                    NOMBRE: element.nomalm
                })
            });
            
        })
    }
    listarCuentas(){
        this.reciboDeCajaService.listar_cuentas({TIPO: null}).then(res => {
            const cuentas = res.filas.recordset
            cuentas.forEach(element => {
                this.dataForm.ARRAY_CUENTAS.push({
                    ID: element.codcue,
                    NOMBRE: element.nomcba
                })
            });
        })
    }
    listarTerceros(){
        this.reciboDeCajaService.listar_terceros({}).then(resp=>{
            this.terceros = resp.filas.recordset
        })
    }
    listarVendedores(){
        this.reciboDeCajaService.listar_vendedores().then(res => {
            this.vendedores = res.filas.recordset
        })
    }
    listarFranquicia(){
        this.reciboDeCajaService.listar_franquicias().then(res => {
            const franquicias = res.filas.recordset
            franquicias.forEach((item)=>{
                this.dataForm.ARRAY_FRANQUICIAS.push({
                    ID: item.CodFranq,
                    NOMBRE: item.NomFranq
                })
            })

        })
    }
    listarFormas(){
        this.reciboDeCajaService.listar_formas_pago().then(res=>{
            for (let i = 0; i < res.filas.recordset.length; i++) {
                const element = res.filas.recordset[i];
                this.dataForm.ARRAY_FORMAS.push({
                    ID: element.codigo,
                    NOMBRE: element.nombre,
                    CODIGO: element.id
                })
                
            }
        })
    }
    listarConceptos(){
        this.reciboDeCajaService.listar_conceptos().then(res => {
            this.conceptos = res.filas.recordset
        })
    }
    modal_terceros(){
        this.ngxSmartModalService.getModal('tercerosModal').open()
    }
    modal_vendedores(){
        this.ngxSmartModalService.getModal('vendedoresModal').open()
    }
    async consecutivo(){
        //const consecutivo = await this.ReciboDeCajaClass.buscar_consecutivo().then(result=>result);
        //this.formData.codigo = consecutivo.codigo;
    }

    async guardar(form: FormControl) {
        this.snotifyService.async('Guardando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const data = Object.assign({}, this.formData);
                    //await this.ReciboDeCajaClass.guardar_almacen(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Almacen gurdado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                    this.ngxSmartModalService.getModal('facturacion_form_modal').close();
                    this.close.emit({ tipo: 2 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo guardar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    async editar(form: FormControl) {
        this.snotifyService.async('Actualizando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const data = Object.assign({}, this.formData);
                    //await this.almacenService.actualizar_almacen(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Almacen actualizado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                    this.ngxSmartModalService.getModal('facturacion_form_modal').close();
                    this.close.emit({ tipo: 3 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo actualizar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    cambiar(i){
        if (this.facturas[i].active) {
            this.calcular()
        }else{
            this.facturas[i].abono = 0;
            this.calcular()
        }
        
    }
    atras(){
        this.router.navigate(['admin/recibo-caja'])
    }
    verificar_accion(data){

    }
    cancel(){
        
    }
    
    async ver_facturas_pendientes(data){
        await this.reciboDeCajaService.listar_facturas_pendientes({IDENTIFICACION_CLIENTE: data}).then(res => {
            let deuda = 0;
            this.facturas = res.filas.recordset
            for (let i = 0; i < res.filas.recordset.length; i++) {
                const element = res.filas.recordset[i];
                deuda += element.total
            }
            this.formData.TOTAL_DEUDAS = deuda
        })
    }
    selectionChangedHandler(event) {
        const data = event.data
        this.formData.ID_CLIENTE = data.id;
        this.formData.IDENTIFICACION_CLIENTE = data.codter;
        this.formData.NOMBRES_CLIENTE = data.nomter;
        this.ver_facturas_pendientes(data.codter)
        this.ngxSmartModalService.getModal('tercerosModal').close()
        this.calcular()
        this.dataGrid.instance.deselectAll()
    }
    selectionChangedHandler2(event) {
        const data = event.data
        this.formData.ID_CLIENTE = data.id;
        this.formData.COBRADOR = data.codter;
        this.formData.NOMBRE_COBRADOR = data.nomven;
        this.ngxSmartModalService.getModal('vendedoresModal').close()
        this.dataGridVendedores.instance.deselectAll()
    }
    selectionChangedHandler3(event) {
        const data = event.data
        
        if(this.list_conceptos.filter(item => item.codcon == data.codcon).length > 0){
            this.snotifyService.warning('Este concepto esta ya listado', 'Ya esta.')
        }else{
            this.list_conceptos.push(data)
        }
        // this.ngxSmartModalService.getModal('conceptosModal').close()
        setTimeout(()=>{
            this.dataGridConceptos.instance.deselectAll()
        }, 1000)
        
    }
    async buscarTercero(e){
        if(e.keyCode === 13){
            const datos = this.terceros.filter(item=> item.codter == this.formData.IDENTIFICACION_CLIENTE)
            if(datos.length > 0){
                const data = datos[0]
                this.formData.ID_CLIENTE = data.id;
                this.formData.IDENTIFICACION_CLIENTE = data.codter;
                this.formData.NOMBRES_CLIENTE = data.nomter;
                await this.ver_facturas_pendientes(data.codter)
                this.calcular()
            }else{
                this.snotifyService.error('No se encontro información del tercero', 'No encontrado!')
            }
            
        }
    }
    verificar_valor_forma(){
        if(this.forma_pago.VALOR >  this.forma_pago.TOTAL_PAGAR){
            this.forma_pago.VALOR = this.forma_pago.TOTAL_PAGAR;
        }
    }
    addForma(forma, franquicia, valor, numero, fecha){
        if (forma != '' || valor != '' && franquicia != undefined) {
            if (valor > 0) {
                if (forma == 'CR' || forma == 'EF' || forma == 'NC' || forma == 'AN' && franquicia == undefined) {
                    var franquicia = forma;
                }
                const nomfor = this.dataForm.ARRAY_FORMAS.filter(item => item.ID == forma)[0].NOMBRE
                const codfor = this.dataForm.ARRAY_FORMAS.filter(item => item.ID == forma)[0].CODIGO
                const nombco = forma;
                var validCH = true;
                if (forma == 'CH' && (numero == '' || numero == null || numero == undefined)) {
                    validCH = false
                }
                if(forma == 'EF'){
                    this.formas_pago_rc.push({
                        codfor: codfor,
                        nomfor: nomfor,
                        nombco: nombco,
                        numero:(numero)?numero:'',
                        fecha: moment(fecha).format('YYYY-MM-DD'),
                        valor: valor
                    })
                }
                this.calcular();
    
            //   if (validCH) {
            //     var dt = $scope.formas_pago.map(function (forma) {
            //       return forma.codfor
            //     }).indexOf(forma)
            //     var dr = $scope.bancos.map(function (banco) {
            //       return banco.codbco
            //     }).indexOf(franquicia)
    
            //     var codfor = $scope.formas_pago[dt].codfor;
            //     var nomfor = $scope.formas_pago[dt].nomfor;
    
            //     if (franquicia == forma) {
            //       var codbco = forma;
            //       var nombco = forma;
            //     } else {
            //       var codbco = $scope.bancos[dr].codbco;
            //       var nombco = $scope.bancos[dr].nombco;
            //     }
    
            //     // var dl = $scope.formas_pago_rc.map(function (forma) {
            //     //   return forma.codfor
            //     // }).indexOf(codfor)
            //     var p = -1
            //     var dl = 0
            //     for (var l = 0; l < this.formas_pago_rc.length; l++) {
            //       var element = this.formas_pago_rc[l];
            //       if (element.codfor == codfor && element.numero == numero) {
            //         dl++
            //         p = l
            //       }
            //     }
    
            //     if (dl == 0) {
            //       this.formas_pago_rc.push({
            //         codfor: codfor,
            //         nomfor: nomfor,
            //         codbco: codbco,
            //         nombco: nombco,
            //         numero: numero,
            //         fecha: moment(fecha).format('YYYY-MM-DD'),
            //         valor: valor
            //       });
            //       calcular()
            //     } else {
            //       $scope.formas_pago_rc[p].valor = parseFloat($scope.formas_pago_rc[p].valor) + parseFloat(valor)
            //       calcular()
            //     }
            //     $scope.forma_pago = {
            //       fecha: moment($scope.recaudo.fecha).format('YYYY-MM-DD'),
            //       forma: null,
            //       numero: null,
            //       franquicia: null,
            //       valor: 0
            //     }
            //     this.ngxSmartModalService.getModal('formadepagoModal').close();
            //   }
            } else {
                this.snotifyService.warning('El valor debe ser mayor a 0', 'Error')
            }
    
          } else {
            this.snotifyService.error('Datos incompletos', 'Error')
          }
          this.forma_pago = new ReciboDeCajaFormasClass()
    }
    eliminardes(i){
        this.list_conceptos.splice(i,1)
        this.calcular();
    }
    eliminar_forma(i){
        this.formas_pago_rc.splice(i,1)
        this.calcular();
    }
    calcular(){
        let totalreciente = 0;
        let total = 0;
        let forma = 0;
        let restante = 0;
        let OTRAS_RETENCIONES = 0;
        let OTROS_DESCUENTO = 0;
        let OTROS_CARGOS = 0;
        let TOTAL_PAGAR = 0;
        
        if(this.formData.ABONO_FACTURA > this.formData.TOTAL_DEUDAS){
           this.formData.ABONO_FACTURA = this.formData.TOTAL_DEUDAS;
        }
        // for (let j = 0; j < this.facturas.length; j++) {
        //     const element = this.facturas[j];
        //     element.abono = (element.abono)?element.abono: 0;
        //     totalreciente += element.abono
        // }
        // console.log(totalreciente)
        for (let i = 0; i < this.facturas.length; i++) {
            const element = this.facturas[i];
            element.abono = (element.abono)?element.abono: 0;
            if(element.active){
                element.abono = (this.formData.ABONO_FACTURA/this.facturas.filter(item => item.active == true).length) + restante
                if(element.total < element.abono){
                    restante += element.abono - element.total
                }
                element.abono = (element.total < element.abono) ? element.total: element.abono;
            }else{ 
                if(element.abono >  element.total ||  total >= this.formData.ABONO_FACTURA){
                    element.abono =  this.formData.ABONO_FACTURA - total;
                }else{
                    element.abono = element.abono;
                }
            } 
            total += element.abono;
        }
        for (let i = 0; i < this.formas_pago_rc.length; i++) {
            const element = this.formas_pago_rc[i];
            forma += element.valor;
        }
        
        // if(total>this.formData.TOTAL_DEUDAS){
        //     this.formData.ABONO_FACTURA = total
        // }
        
        this.recibocaja.FORMAS_PAGO = forma;
        this.recibocaja.TOTAL_PAGAR = total + this.recibocaja.OTROS_CARGOS - this.recibocaja.DESCUENTOS_PRONTO_PAGO - this.recibocaja.OTROS_DESCUENTO;
        this.recibocaja.NUEVO_SALDO = this.formData.TOTAL_DEUDAS - this.forma_pago.TOTAL_PAGAR;
        //-----OTROS DESCUENTOS, CARGOS Y RETENCIONES-----//
        for (var j = 0; j < this.list_conceptos.length; j++) {
            var element = this.list_conceptos[j];
            if (element.tipcon == 2) {
              OTROS_CARGOS += element.valco
            //   TOTAL_PAGAR += element.valco
            } else if (element.tipcon == 3) {
              element.valco = (element.base * (parseFloat(element.porcon) / 100))
              OTROS_DESCUENTO += element.valco
            //   TOTAL_PAGAR -= element.valco
            } else {
              element.valco = (element.base * (parseFloat(element.porcon) / 100))
              OTRAS_RETENCIONES += element.valco
            //   TOTAL_PAGAR -= element.valco
            }
        }
        this.recibocaja.OTROS_CARGOS = OTROS_CARGOS
        this.recibocaja.OTROS_DESCUENTO = OTROS_DESCUENTO
        this.recibocaja.OTRAS_RETENCIONES = OTRAS_RETENCIONES
        this.recibocaja.TOTAL_PAGAR = this.recibocaja.TOTAL_PAGAR + this.recibocaja.OTROS_CARGOS - this.recibocaja.OTROS_DESCUENTO - this.recibocaja.OTRAS_RETENCIONES
        this.recibocaja.NUEVO_SALDO = this.formData.TOTAL_DEUDAS -  this.recibocaja.ABONO_FACTURA;
        this.recibocaja.DIFERENCIA = (this.recibocaja.TOTAL_PAGAR - this.recibocaja.FORMAS_PAGO);
        this.forma_pago.TOTAL_PAGAR = this.recibocaja.TOTAL_PAGAR
        
        if (this.recibocaja.DIFERENCIA < 0 && this.recibocaja.NUEVO_SALDO > 0) {} else if (this.recibocaja.DIFERENCIA < 0 && this.recibocaja.NUEVO_SALDO == 0) {
            this.recibocaja.SALDO_FAVOR = Math.abs(this.recibocaja.DIFERENCIA)
        } else {
            this.recibocaja.SALDO_FAVOR = 0;
        }
    }
    async ver_factura(id){
        const doc = await this.facturacionMobilsoftService.reportePDFFactura({id_factura: id})
        openPdfMake(doc);
    }

    modal_formas_pago(){
        this.ngxSmartModalService.getModal('formadepagoModal').open();
        this.calcular()
    }
    modal_conceptos(){
        this.ngxSmartModalService.getModal('conceptosModal').open();
    }

    async generar_recibo(form: FormControl){
        
        await this.snotifyService.async('Se esta guardando la información', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    await setTimeout(async ()=>{
                        resolve({
                            title: 'Exito',
                            body: 'Guardado correctamente.',
                            config: {
                                showProgressBar: true,
                                closeOnClick: true,
                                timeout: 2000
                            }
                        })
                        await this.generar_reporte()
                        // form.reset()
                    }, 3000)
                    
                    } catch (error) {
                        reject({
                            title: 'Error!!!',
                            body: 'No se puedo guardar la información.',
                            config: { closeOnClick: true }
                        })
                    }
                })
            );
        
       
    }
    async generar_reporte(){
        await this.snotifyService.async('Se esta generando el reporte', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const doc = await   this.reciboDeCajaService.reportePDFFactura({   
                                            formData: this.formData, 
                                            recibocaja: this.recibocaja, 
                                            facturas: this.facturas
                                        }).then(res=>res)
                    openPdfMake(doc)
                    resolve({
                        title: 'Exito',
                        body: 'Se genero correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 5000
                        }
                    })

                    } catch (error) {
                        reject({
                            title: 'Error!!!',
                            body: 'No se puedo generar el reporte',
                            config: { closeOnClick: true }
                        })
                    }
                })
            );
    }
    total_valor(){

        if(this.forma_pago.FORMA == 'EF'){
            this.forma_pago.VALOR = this.forma_pago.TOTAL_PAGAR
        }
    }
    
}