export class ReciboDeCajaClass {
    constructor(
        public ID?: number,
        public NIT_EMPRESA?: string,
        public RAZON_SOCIAL?: string,
        public FECHA?: string,
        public NUMERO_COMPROBANTE?: string,
        public CONSIGNAR_CUENTA?: string,
        public ALMACEN?: number,
        public ID_CLIENTE?: number,
        public IDENTIFICACION_CLIENTE?: string,
        public NOMBRES_CLIENTE?: string,
        public DEPARTAMENTO?: number,
        public COBRADOR?: string,
        public NOMBRE_COBRADOR?: string,
        public ABONO_FACTURA?: number,
        public TOTAL_DEUDAS?: number,
        public DESCRIPCION?: string
    ) {
        const month = ((new Date().getMonth()+1)<=9)? '0'+new Date().getMonth()+1: new Date().getMonth()+1
        const date = ((new Date().getDate()<=9))? '0'+new Date().getDate(): new Date().getDate()
        this.ID = null;
        this.NIT_EMPRESA = null;
        this.RAZON_SOCIAL = null;
        this.FECHA = new Date().getFullYear()+'-'+month+'-'+date;
        this.NUMERO_COMPROBANTE = null;
        this.CONSIGNAR_CUENTA = null;
        this.ALMACEN = null;
        this.IDENTIFICACION_CLIENTE = null;
        this.NOMBRES_CLIENTE = null;
        this.DEPARTAMENTO = null;
        this.COBRADOR = null;
        this.NOMBRE_COBRADOR = null;
        this.ABONO_FACTURA = 0;
        this.TOTAL_DEUDAS = 0;
        this.DESCRIPCION = null;
        this.ID_CLIENTE = null;
    }
}
export class ReciboDeCajaTotalesClass {
    constructor(
        public SALDO?: number,
        public ABONO_FACTURA?: number,
        public DESCUENTOS_PRONTO_PAGO?: number,
        public OTRAS_RETENCIONES?: number,
        public OTROS_DESCUENTO?: number,
        public OTROS_CARGOS?: number,
        public TOTAL_PAGAR?: number,
        public FORMAS_PAGO?: number,
        public DIFERENCIA?: number,
        public SALDO_FAVOR?: number,
        public NUEVO_SALDO?: number
    ) {
        this.SALDO = 0;
        this.ABONO_FACTURA = 0;
        this.DESCUENTOS_PRONTO_PAGO = 0;
        this.OTRAS_RETENCIONES = 0;
        this.OTROS_DESCUENTO = 0;
        this.OTROS_CARGOS = 0;
        this.TOTAL_PAGAR = 0;
        this.FORMAS_PAGO = 0;
        this.DIFERENCIA = 0;
        this.SALDO_FAVOR  = 0;
        this.NUEVO_SALDO = 0;
    }
}
export class ReciboDeCajaFormasClass {
    constructor(
        public TOTAL_PAGAR?: number,
        public FORMAS_PAGO?: number,
        public FORMA?: string,
        public VALOR?: number,
        public FRANQUICIA?: number,
        public NUMERO?: number,
        public FECHA?: string
    ) {
        const mes = ((new Date().getMonth()+1)<=9)?'0'+new Date().getMonth()+1: new Date().getMonth()+1
        const dia = ((new Date().getDate())<=9)?'0'+new Date().getDate(): new Date().getDate()
        this.TOTAL_PAGAR = 0;
        this.FORMAS_PAGO = 0;
        this.FORMA = null;
        this.VALOR = 0;
        this.FRANQUICIA = null;
        this.FECHA = new Date().getFullYear()+'-'+mes+'-'+dia
    }
}

export class FacturacionArraysClass {
    constructor(
        public ARRAY_ALMACEN?: Array<any>,
        public ARRAY_CUENTAS?: Array<any>,
        public ARRAY_FORMAS?: Array<any>,
        public ARRAY_FRANQUICIAS?: Array<any>
    ) {
        this.ARRAY_ALMACEN = [{
            ID: null,
            NOMBRE: 'Seleccione un almacen'
        }]
        this.ARRAY_CUENTAS = [{
            ID: null,
            NOMBRE: 'Seleccione una cuenta'
        }]
        this.ARRAY_FORMAS = [{
            ID: null,
            NOMBRE: 'Seleccione una forma de pago',
            CODIGO:null
        }]
        this.ARRAY_FRANQUICIAS = [{
            ID: null,
            NOMBRE: 'Seleccione una franquicia'
        }]
    }
}