import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP } from '@app/constant';

@Injectable({
    providedIn: 'root'
})
export class EgresosService {
    apiEndpoint = APP.ApiEndpoint;
    AppBaseUrl = APP.AppBaseUrl;
    constructor(
        private http: HttpClient,
    ) { }
    async listar_empresa(): Promise<any> {
        const url = `${this.apiEndpoint}/api/empresa/listar`;
        return await this.http.get<any>(url).toPromise();
    }
    async listar_almacenes(): Promise<any>{
        const url = `${this.apiEndpoint}/api/almacenes/listar`;
        return await this.http.get<any>(url).toPromise();
    }
    async listar_cuentas(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/cuentas/listar`;
        return await this.http.post<any>(url, data).toPromise();
    }
    async listar_terceros(data: any): Promise<any> {
        const url = `${this.apiEndpoint}/api/terceros/listar`;
        return await this.http.get<any>(url).toPromise();
    }
    async listar_formas_pago(): Promise<any>{
        const url = `${this.apiEndpoint}/api/facturacion-mobilsoft/listar_formas_pago`;
        return await this.http.get<any>(url).toPromise();
    }
    async listar_conceptos(): Promise<any>{
        const url = `${this.apiEndpoint}/api/concepto/listar`;
        return await this.http.get<any>(url).toPromise();
    }

}
