import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { EgresosService } from './egresos.service';
import { Router } from '@angular/router'

@Component({
    selector: 'app-egresos',
    templateUrl: './egresos.component.html',
    styleUrls: ['./egresos.component.css']
})
export class EgresosComponent implements OnInit {

    c_id_table: number;
    c_accion_form: number;
    c_cambio_table: number;
    f_state_table: number;

    constructor(
        private egresosService: EgresosService,
        private titleService: Title,
        private route: Router
    ) {
        this.titleService.setTitle('Egresos');
        this.c_accion_form = 0;
        this.c_cambio_table = 1;
        this.f_state_table = 1;
    }

    ngOnInit() {
    }

    newEgresos() {
        this.route.navigate(['admin/egresos/new'])
    }
    event_table_egresos(event: any): void {
        switch (event.tipo) {
            case 1: //:: Nuevo
                this.c_accion_form = event.tipo;
                break;
            case 2: //:: Editar
                this.c_accion_form = event.tipo;
                this.c_id_table = event.id;
                break;
            case 3: //:: Desactivado
                this.c_cambio_table++;
                break;
            case 4: //:: Reactivado
                this.c_cambio_table++;
                break;
        }
    }

    event_form_egresos(event): void {
        switch (event.tipo) {
            case 1: //:: Cerró

                break;
            case 2: //:: Guardó
                this.c_cambio_table++;
                break;
            case 3: //:: Editó
                this.c_cambio_table++;
                break;
        }
        this.c_accion_form = 0;
        this.c_id_table = null;
    }

}
