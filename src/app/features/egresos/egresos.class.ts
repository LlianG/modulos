export class EgresosClass {
    constructor(
        public ID?: number,
        public NUMERO_CUENTA?: string,
        public NUMERO_CUENTA_CONTABLE?: string,
        public NIT_PROVEEDOR?: string,
        public NIT_EMPRESA?: string,
        public RAZON_SOCIAL?: string,
        public RAZON_SOCIAL_PROVEEDOR?: string,
        public TELEFONO_PROVEEDOR?: string,
        public DIRECCION_PROVEEDOR?: string,
        public FECHA?: String,
        public NUMERO_REFERENCIA?: string,
        public ALMACEN?: number,
        public DESCRIPCION?: string,
        public FORMA_PAGO?: number,
        public DOCUMENTO?: string,
        public TOTAL_DEUDA?: number,
        public TOTAL_ABONO?: number,
        public DIFERENCIA?: number,
        public CUENTA?: number,
    ) {
        const month = ((new Date().getMonth()+1)<=9)? '0'+new Date().getMonth()+1: new Date().getMonth()+1
        const date = ((new Date().getDate()<=9))? '0'+new Date().getDate(): new Date().getDate()
        this.ID = null;
        this.NIT_EMPRESA = null;
        this.NUMERO_CUENTA = null;
        this.NUMERO_CUENTA_CONTABLE = null;
        this.NIT_PROVEEDOR = null;
        this.RAZON_SOCIAL_PROVEEDOR = null;
        this.RAZON_SOCIAL = null;
        this.TELEFONO_PROVEEDOR = null;
        this.DIRECCION_PROVEEDOR = null;
        this.FORMA_PAGO = null;
        this.FECHA = new Date().getFullYear()+'-'+month+'-'+date;
        this.NUMERO_REFERENCIA = null;
        this.ALMACEN = null;
        this.CUENTA = null;
        this.DESCRIPCION = null;
        this.DOCUMENTO = null;
        this.TOTAL_DEUDA = 0;
        this.TOTAL_ABONO = 0;
        this.DIFERENCIA = 0;

    }
}
export class EgresosArraysClass {
    constructor(
        public ARRAY_ALMACEN?: Array<any>,
        public ARRAY_CUENTAS?: Array<any>,
        public ARRAY_FORMAS?: Array<any>
    ) {
        this.ARRAY_ALMACEN = [{
            ID: null,
            NOMBRE: 'Seleccione un almacen'
        }]
        this.ARRAY_CUENTAS = [{
            ID: null,
            NOMBRE: 'Seleccione una cuenta'
        }]
        this.ARRAY_FORMAS = [{
            ID: null,
            NOMBRE: 'Seleccione una forma de pago',
            CODIGO:null
        }]
    }
}
export class EgresosResumenClass {
    constructor(
        public NETO_EGRESO?: number,
        public ABONO_FACTURAS?: number,
        public OTROS_DESCUENTOS?: number,
        public RETENCIONES?: number,
        public OTROS_CARGOS?: number,
        public SOBRANTE?: number,
        public TOTAL_ABONO?: number,
        public TOTAL_PAGAR?: number,
        public NUEVO_SALDO?: number,
        public ANTICIPOS?: number,
        public OTRAS_RETENCIONES?: number,
        public OTROS_DESCUENTO?: number,
    ){
        this.NETO_EGRESO = 0;
        this.ABONO_FACTURAS = 0;
        this.OTROS_DESCUENTOS = 0;
        this.RETENCIONES = 0;
        this.OTROS_CARGOS = 0;
        this.TOTAL_PAGAR = 0;
        this.SOBRANTE = 0;
        this.TOTAL_ABONO = 0;
        this.NUEVO_SALDO = 0;
        this.ANTICIPOS = 0;
        this.OTRAS_RETENCIONES = 0;
        this.OTROS_DESCUENTO = 0;
    }
}