import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormControl } from '@angular/forms';
import { EgresosArraysClass, EgresosClass, EgresosResumenClass } from '../egresos.class';
import { DxDataGridComponent } from 'devextreme-angular';
import { EgresosService } from '../egresos.service';
import { SnotifyService } from 'ng-snotify';
import { Router } from '@angular/router';
import { faEquals,faPlus, faHandScissors, faPercent, faMoneyBillWave, faMinus, faCreditCard } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-egresos-form',
    templateUrl: 'egresos.form.component.html',
    styleUrls: ['egresos.form.component.css']
})

export class EgresosFormComponent implements OnInit {
    @ViewChild('gridProveedor') dataGrid: DxDataGridComponent;
    @ViewChild('gridConceptos') dataGridConceptos: DxDataGridComponent;
    getSelectedRowsDataConceptos() {
        return this.dataGridConceptos.instance.getSelectedRowsData()[0]
    }
    getSelectedRowsData() {
        return this.dataGrid.instance.getSelectedRowsData()[0]
    }
    c_accion: number;
    
    tipFacRap: number;
    formData: EgresosClass;
    dataForm: EgresosArraysClass;
    totalTonelaje: number;
    valorNotaBancaria: number;
    egreso: EgresosResumenClass;
    TipoEgr: number;
    selecct: boolean;
    proveedor: any;
    list_conceptos: any;
    conceptos: any;
    icons = {faequals:faEquals, faplus:faPlus, faHandscissors: faHandScissors, faPercent:faPercent, faMoneyBillWave:faMoneyBillWave, faMinus:faMinus, faCreditCard: faCreditCard}
    icon1style = {
        'font-size': '17px',
        'position': 'absolute',
        'top': '18px',
        'left': '165px',
        'color': '#42c18787'
    }
    facturas_pendientes = [];
    list_factura = []
    @Input() set accion(accion: number) {
        this.c_accion = accion;
        switch (accion) {
            case 1: //:: nuevo
                this.formData = new EgresosClass();
                this.consecutivo();
                break;
            case 2: //:: editar
                // this.ngxSmartModalService.getModal('facturacion_form_modal').open();
                break;
        }
    }
    @Input() set idIn(id: number) {
        if (id) { //:: editar
            this.formData = new EgresosClass();
            // this.almacenService.listar_almacen(id).then(data => {
            //     this.formData = data;
            // })
        }
    }
    @Output() close = new EventEmitter();


    constructor(
        public ngxSmartModalService: NgxSmartModalService,
        private snotifyService: SnotifyService,
        private router:Router,
        private egresosService: EgresosService
    ) {
        this.formData = new EgresosClass();
        this.dataForm = new EgresosArraysClass();
        this.tipFacRap = 1;
        this.totalTonelaje = 0;
        this.valorNotaBancaria = 0;
        this.egreso = new EgresosResumenClass();
        this.TipoEgr = 0;
        this.selecct = false;
        this.proveedor=[]
        this.list_conceptos = []
        this.facturas_pendientes = [
            {
                comfac: '0000000001',
                docfac: 'FC',
                fecha: '10/09/2019',
                fecha_vencimiento: '10/10/2019',
                valfac: 2000000,
                abofac: 0,
                devfac: 0,
                saldo: 0,
            },
            {
                comfac: '0000000002',
                docfac: 'FC',
                fecha: '01/09/2019',
                fecha_vencimiento: '01/10/2019',
                valfac: 1607000,
                abofac: 0,
                devfac: 0,
                saldo: 0,
            },
            {
                comfac: '0000000003',
                docfac: 'FC',
                fecha: '12/09/2019',
                fecha_vencimiento: '12/10/2019',
                valfac: 415000,
                abofac: 100000,
                devfac: 0,
                saldo: 0,
            },

        ]
    }

    ngOnInit() { 
        this.listarEmpresa()
        this.listarAlmacenes()
        this.listarCuentas()
        this.listarTerceros()
        this.listarFormas()
        this.listarConceptos()
    }
    listarEmpresa(){
        this.egresosService.listar_empresa().then(res => {
            this.formData.NIT_EMPRESA = res.filas.recordset[0].nitemp
            this.formData.RAZON_SOCIAL = res.filas.recordset[0].razemp
        })
    }
    listarAlmacenes(){
        this.egresosService.listar_almacenes().then(res => {
            const alamacen = res.filas.recordset
            alamacen.forEach(element => {
                this.dataForm.ARRAY_ALMACEN.push({
                    ID: element.id,
                    NOMBRE: element.nomalm
                })
            });
            
        })
    }
    listarCuentas(){
        this.egresosService.listar_cuentas({TIPO: null}).then(res => {
            const cuentas = res.filas.recordset
            cuentas.forEach(element => {
                this.dataForm.ARRAY_CUENTAS.push({
                    ID: element.codcue,
                    NOMBRE: element.nomcba,
                    NUMERO: element.numcba
                })
            });
        })
    }
    listarTerceros(){
        this.egresosService.listar_terceros({}).then(resp=>{
            this.proveedor = resp.filas.recordset
        })
    }

    listarFormas(){
        this.egresosService.listar_formas_pago().then(res=>{
            for (let i = 0; i < res.filas.recordset.length; i++) {
                const element = res.filas.recordset[i];
                this.dataForm.ARRAY_FORMAS.push({
                    ID: element.codigo,
                    NOMBRE: element.nombre,
                    CODIGO: element.id
                })
                
            }
        })
    }
    listarConceptos(){
        this.egresosService.listar_conceptos().then(res => {
            this.conceptos = res.filas.recordset
        })
    }
    numero_cuenta(data){
        const datos = this.dataForm.ARRAY_CUENTAS.filter(item => item.ID == this.formData.CUENTA)
        if(datos.length > 0){
            this.formData.NUMERO_CUENTA = datos[0].NUMERO
            this.formData.NUMERO_CUENTA_CONTABLE = datos[0].ID
        }
    }
    cerrar() {
        this.ngxSmartModalService.getModal('facturacion_form_modal').close();
        this.close.emit({ tipo: 1 });
    }

    async consecutivo(){
        //const consecutivo = await this.RequisicionClass.buscar_consecutivo().then(result=>result);
        //this.formData.codigo = consecutivo.codigo;
    }

    async guardar(form: FormControl) {
        this.snotifyService.async('Guardando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const data = Object.assign({}, this.formData);
                    //await this.RequisicionClass.guardar_almacen(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Almacen gurdado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                    this.ngxSmartModalService.getModal('facturacion_form_modal').close();
                    this.close.emit({ tipo: 2 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo guardar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    async editar(form: FormControl) {
        this.snotifyService.async('Actualizando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const data = Object.assign({}, this.formData);
                    //await this.almacenService.actualizar_almacen(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Almacen actualizado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                    this.ngxSmartModalService.getModal('facturacion_form_modal').close();
                    this.close.emit({ tipo: 3 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo actualizar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    atras(){
        this.router.navigate(['admin/egresos'])
    }
    verificar_accion(data){

    }
    cancel(){
        
    }
    calcular(){
        let OTRAS_RETENCIONES = 0;
        let OTROS_DESCUENTO = 0;
        let OTROS_CARGOS = 0;
        let TOTAL = 0;
        let restante = 0;
        let saldo_actual = this.formData.TOTAL_ABONO;
        if(this.formData.TOTAL_ABONO > this.formData.TOTAL_DEUDA){
            this.formData.TOTAL_ABONO = this.formData.TOTAL_DEUDA
        }
        this.formData.DIFERENCIA = this.formData.TOTAL_DEUDA - this.formData.TOTAL_ABONO
        for (let i = 0; i < this.list_factura.length; i++) {
            const element = this.list_factura[i];
            element.abono = (element.abono)?element.abono:0
            if(element.active){
                element.abono = saldo_actual + restante
            }
            if(element.valfac < element.abono){
                element.abono = element.valfac
                saldo_actual = saldo_actual - element.abono
                restante += element.valfac - element.abono
            }else{
                element.abono = element.abono
            }
            element.abono = (element.abono)?element.abono:0;
            // console.log(element.abofac)
            TOTAL += element.abono
            
        }
        for (var j = 0; j < this.list_conceptos.length; j++) {
            var element = this.list_conceptos[j];
            if (element.tipcon == 2) {
              OTROS_CARGOS += element.valco
            } else if (element.tipcon == 3) {
              element.valco = (element.base * (parseFloat(element.porcon) / 100))
              OTROS_DESCUENTO += element.valco
            } else {
              element.valco = (element.base * (parseFloat(element.porcon) / 100))
              OTRAS_RETENCIONES += element.valco
            }
        }
        this.egreso.OTROS_CARGOS = OTROS_CARGOS;
        this.egreso.OTROS_DESCUENTO = OTROS_DESCUENTO;
        this.egreso.OTRAS_RETENCIONES = OTRAS_RETENCIONES;
        this.egreso.TOTAL_ABONO = TOTAL
        this.egreso.TOTAL_PAGAR = this.egreso.TOTAL_ABONO + OTROS_CARGOS - OTROS_DESCUENTO - OTRAS_RETENCIONES;
    }
    eliminardes(i){
        this.list_conceptos.splice(i,1)
        this.calcular();
    }
    cambio_tipo_egreso(event){
        this.TipoEgr = (event.target.checked)?1:0
    }
    selectionChangedHandler(data){
        let TOTAL_DEUDA = 0;
        this.formData.RAZON_SOCIAL_PROVEEDOR = data.data.nomter
        this.formData.NIT_PROVEEDOR = data.data.codter
        this.formData.TELEFONO_PROVEEDOR = data.data.telter
        this.formData.DIRECCION_PROVEEDOR = data.data.dirter
        this.list_factura = this.facturas_pendientes;
        for (let i = 0; i < this.list_factura.length; i++) {
            const element = this.list_factura[i];
            TOTAL_DEUDA += (element.valfac - element.abofac) 
        }
        this.formData.TOTAL_DEUDA = TOTAL_DEUDA;
        this.ngxSmartModalService.getModal('proveedorModal').close()
    }
    selectionChangedHandler3(event) {
        const data = event.data
        
        if(this.list_conceptos.filter(item => item.codcon == data.codcon).length > 0){
            this.snotifyService.warning('Este concepto esta ya listado', 'Ya esta.')
        }else{
            this.list_conceptos.push(data)
        }
        // this.ngxSmartModalService.getModal('conceptosModal').close()
        setTimeout(()=>{
            this.dataGridConceptos.instance.deselectAll()
        }, 1000)
        
    }
    modal_proveedor(){
        this.ngxSmartModalService.getModal('proveedorModal').open()
    }
    modal_conceptos(){
        this.ngxSmartModalService.getModal('conceptosModal').open();
    }
    cambiar(i){
        if (this.list_factura[i].active) {
            this.calcular()
        }else{
            this.list_factura[i].abono = 0;
            this.calcular()
        }
    }
}