export class DevolucionesClientesClass {
    constructor(
        public ID?: number,
        public NIT_EMPRESA?: string,
        public RAZON_SOCIAL?: string,
        public FECHA?: Date,
        public NUMERO_REFERENCIA?: string,
        public ALMACEN?: number,
        public DESCRIPCION?: string,
        public DOCUMENTO?: string,
        public CONCEPTO?: string,
        public TELEFONO_PROVEEDOR?: string,
        public CODIGO_FACTURA?: string
    ) {
        this.ID = null;
        this.NIT_EMPRESA = null;
        this.RAZON_SOCIAL = null;
        this.FECHA = new Date();
        this.NUMERO_REFERENCIA = null;
        this.ALMACEN = null;
        this.DESCRIPCION = null;
        this.DOCUMENTO = null;
        this.CONCEPTO = null;
        this.TELEFONO_PROVEEDOR = null;
        this.CODIGO_FACTURA = null;

    }
}
export class DevolucionesClientesArraysClass {
    constructor(
        public ARRAY_ALMACEN?: Array<any>,
        public ARRAY_CUENTAS?: Array<any>,
    ) {
        this.ARRAY_ALMACEN = [{
            ID: null,
            NOMBRE: 'Seleccione un almacen'
        }]
        this.ARRAY_CUENTAS = [{
            ID: null,
            NOMBRE: 'Seleccione una cuenta'
        }]
    }
}

export class DevolucionesClientesDataClass {
    constructor(
        public valor?: number,
        public abonos?: number,
        public devoluciones?: number,
        public saldo_hoy?: number,
        public saldo?: number,
    ){
        this.valor = 0
        this.abonos = 0
        this.devoluciones = 0
        this.saldo_hoy = 0
        this.saldo = 0

    }
}
export class DevolucionesClientesResumenClass{
    constructor(
        public subtotal?: number,
        public subtotal_neg?: number,
        public descuentos?: number,
        public descuentos_neg?: number,
        public cargos?: number,
        public cargos_neg?: number,
        public iva?: number,
        public iva_neg?: number,
        public retefuente?: number,
        public retefuente_neg?: number,
        public reteica?: number,
        public reteica_neg?: number,
        public reteiva?: number,
        public reteiva_neg?: number,
        public total?: number,
        public total_neg?: number,
        public nuevo_saldo?: number,
        public nuevo_saldo_neg?: number,
        public saldo_favor?: number,
    ){
        this.subtotal = 0;
        this.subtotal_neg = 0;
        this.descuentos = 0;
        this.descuentos_neg = 0;
        this.cargos = 0;
        this.cargos_neg = 0;
        this.iva = 0;
        this.iva_neg = 0;
        this.retefuente = 0;
        this.retefuente_neg = 0;
        this.reteica = 0;
        this.reteica_neg = 0;
        this.reteiva = 0;
        this.reteiva_neg = 0;
        this.total = 0;
        this.total_neg = 0;
        this.nuevo_saldo = 0;
        this.nuevo_saldo_neg = 0;
        this.saldo_favor = 0;
    }
    
}