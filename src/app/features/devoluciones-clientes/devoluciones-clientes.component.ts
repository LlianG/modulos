import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { DevolucionesClientesService } from './devoluciones-clientes.service';
import { Router } from '@angular/router'

@Component({
    selector: 'app-devoluciones-clientes',
    templateUrl: './devoluciones-clientes.component.html',
    styleUrls: ['./devoluciones-clientes.component.css']
})
export class DevolucionesClientesComponent implements OnInit {

    c_id_table: number;
    c_accion_form: number;
    c_cambio_table: number;
    f_state_table: number;

    constructor(
        private consignacionesService: DevolucionesClientesService,
        private titleService: Title,
        private route: Router
    ) {
        this.titleService.setTitle('Devoluciones a clientes');
        this.c_accion_form = 0;
        this.c_cambio_table = 1;
        this.f_state_table = 1;
    }

    ngOnInit() {
    }

    newDevolucion() {
        this.route.navigate(['admin/devoluciones-clientes/new'])
    }
    event_table_devoluciones_clientes(event: any): void {
        switch (event.tipo) {
            case 1: //:: Nuevo
                this.c_accion_form = event.tipo;
                break;
            case 2: //:: Editar
                this.c_accion_form = event.tipo;
                this.c_id_table = event.id;
                break;
            case 3: //:: Desactivado
                this.c_cambio_table++;
                break;
            case 4: //:: Reactivado
                this.c_cambio_table++;
                break;
        }
    }

    event_form_devoluciones_clientes(event): void {
        switch (event.tipo) {
            case 1: //:: Cerró

                break;
            case 2: //:: Guardó
                this.c_cambio_table++;
                break;
            case 3: //:: Editó
                this.c_cambio_table++;
                break;
        }
        this.c_accion_form = 0;
        this.c_id_table = null;
    }

}
