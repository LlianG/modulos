import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { DevolucionesClientesClass, DevolucionesClientesArraysClass } from '../devoluciones-clientes.class';
import { DevolucionesClientesService } from '../devoluciones-clientes.service';
import { Subject } from 'rxjs';
import { SnotifyService } from 'ng-snotify';

@Component({
    selector: 'app-devoluciones-clientes-table',
    templateUrl: 'devoluciones-clientes.table.component.html',
    styleUrls: ['devoluciones-clientes.table.component.css']
})

export class DevolucionesClientesTableComponent implements OnInit {
    stateFilter: number;
    listadoFacturaciones: DevolucionesClientesClass[];
    @Input() set state(state: number) {
        this.stateFilter = state;
        //this.listar_datos();
    }
    @Input() set cambio(cambio: number) {
        if (cambio > 1) {
            //this.listar_datos();
        }
    }
    @Output() close = new EventEmitter();

    constructor(
        private consignacionesService: DevolucionesClientesService,
        private snotifyService: SnotifyService
    ) {
    }

    ngOnInit() { }


    async listar_datos() {
        // this.listadoAlmacenes = await this.almacenService.listar_almacenes({ estado: this.stateFilter }).then(result => result);
        // this.rerenderTable();
    }
    async editar(id: number) {
        this.close.emit({ tipo: 2, id: id });
    }
    async desactivar(id: number) {
        this.snotifyService.async('Actualizando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    // await this.almacenService.cambiar_estado(id, 0);
                    this.close.emit({ tipo: 3 });
                    resolve({
                        title: 'Exito',
                        body: 'Almacen desactivado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo desactivar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    async reactivar(id: number) {
        this.snotifyService.async('Actualizando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    //await this.almacenService.cambiar_estado(id, 1);
                    this.close.emit({ tipo: 4 });
                    resolve({
                        title: 'Exito',
                        body: 'Almacen reactivado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo reactivar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
}