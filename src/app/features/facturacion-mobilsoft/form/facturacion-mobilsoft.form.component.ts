import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NgxSmartModalService } from 'ngx-smart-modal';
import { FormControl } from '@angular/forms';
import { FacturacionClass, FacturacionTotalesClass, FacturacionCreditoClass, FacturacionArraysClass, FacturacionTableClass } from '../facturacion-mobilsoft.class';
import { FacturacionMobilsoftService } from '../facturacion-mobilsoft.service';
import { SnotifyService } from 'ng-snotify';
import { Router } from '@angular/router';
import { DxDataGridComponent } from 'devextreme-angular';
import { openPdfMake } from '@app/core/pdfmake.config';


@Component({
    selector: 'app-facturacion-mobilsoft-form',
    templateUrl: 'facturacion-mobilsoft.form.component.html',
    styleUrls: ['facturacion-mobilsoft.form.component.css']
})

export class FacturacionMobilsoftFormComponent implements OnInit {
    @ViewChild('gridTerceros') dataGrid: DxDataGridComponent;
    @ViewChild('gridProductos') dataGridProductos: DxDataGridComponent;
    
    getSelectedRowsData() {
        return this.dataGrid.instance.getSelectedRowsData()[0]
    }

    c_accion: number;
    tipFacRap: number;
    TipoFac: number;
    facturacion: FacturacionTotalesClass;
    cupo_credito: FacturacionCreditoClass;
    formData: FacturacionClass;
    dataForm: FacturacionArraysClass;
    dataTable: FacturacionTableClass;
    valCorreo: boolean;
    terceros: any;
    products: any =[];
    ciudades:any;
    departamentos: any;
    listado_precios: any;
    tipval_fac: number;
    servicios: any;

    @Input() set accion(accion: number) {
        this.c_accion = accion;
        switch (accion) {
            case 1: //:: nuevo
                this.formData = new FacturacionClass();
                this.consecutivo();
                this.ngxSmartModalService.getModal('facturacion_form_modal').open();
                break;
            case 2: //:: editar
                this.ngxSmartModalService.getModal('facturacion_form_modal').open();
                break;
        }
    }
    @Input() set idIn(id: number) {
        if (id) { //:: editar
            this.formData = new FacturacionClass();
            // this.almacenService.listar_almacen(id).then(data => {
            //     this.formData = data;
            // })
        }
    }
    @Output() close = new EventEmitter();


    constructor(
        public ngxSmartModalService: NgxSmartModalService,
        private snotifyService: SnotifyService,
        private router:Router,
        private facturacionMobilsoftService: FacturacionMobilsoftService
    ) {
        this.formData = new FacturacionClass();
        this.facturacion = new FacturacionTotalesClass();
        this.cupo_credito = new FacturacionCreditoClass();
        this.dataForm = new FacturacionArraysClass();
        this.dataTable = new FacturacionTableClass();
        this.tipFacRap = 1;
        this.TipoFac = 1;
        this.valCorreo = true;
        this.tipval_fac = 2;
        this.servicios = [];
    }

    ngOnInit() { 
        this.listarTerceros();
        this.listarDepartamento();
        this.listarCiudades();
        this.listarAlmacenes();
        this.listarCuentas();
        this.listarServicios();
        this.listarFormasPago();
    }

    buscarTercero(e){
        if(e.keyCode === 13){
            const datos = this.terceros.filter(item=> item.codter == this.formData.IDENTIFICACION_CLIENTE)
            if(datos.length > 0){
                const data = datos[0]
                this.formData.ID_CLIENTE = data.id;
                this.formData.IDENTIFICACION_CLIENTE = data.codter;
                this.formData.NOMBRES_CLIENTE = data.nomter;
                this.formData.DIRECCION = data.dirter;
                this.formData.TELEFONO = data.telter;
                this.formData.CELULAR = data.celter;
                this.formData.CIUDAD = (data.coddane.trim() == '')? '08001':data.coddane;
                this.formData.CORREO = data.email;
                this.cupo_credito.AUTORIZADO = data.cupo_credito;
                this.cupo_credito.DISPONIBLE = this.cupo_credito.AUTORIZADO - this.cupo_credito.UTILIZADO
                this.formData.PLAZO = data.plazo;
                this.formData.DEPARTAMENTO = (this.ciudades.filter(item => item.coddane.trim() == data.coddane.trim()).length > 0)?this.ciudades.filter(item => item.coddane.trim() == data.coddane.trim())[0].coddep: null
                this.ngxSmartModalService.getModal('myModal').close()
                this.calcular_fecha()
            }else{
                this.snotifyService.error('No se encontro información del tercero', 'No encontrado!')
            }
            
        }
    }
    listarServicios(){
        this.facturacionMobilsoftService.listar_servicios({}).then(resp=>{
            const servicios = resp.filas.recordsets[0];
            servicios.forEach(element =>{
                this.dataForm.ARRAY_SERVICIOS.push({
                    ID: element.id,
                    CODIGO: element.codins,
                    NOMBRE: element.nomins,
                    DESCRIPCION: element.desins,
                    IVA: element.ivains,
                    VALOR: element.valins,
                    UNIDAD: element.medins,
                    CANTIDAD_UNIDAD: element.numins
                })
            })
        })
    }
    listarFormasPago(){
        this.facturacionMobilsoftService.listar_formas_pago().then(res=>{
            const formas_pago = res.filas.recordset
            formas_pago.forEach((item)=>{
                this.dataForm.ARRAY_MODALIDAD_PAGO.push({
                    ID: item.codigo,
                    NOMBRE: item.nombre
                })
            })
        })
    }
    listaPrecios(){
        this.facturacionMobilsoftService.lista_precios({
            codalm: this.formData.ALMACEN,
            tiptar: 1,
            accion: 'L'
        }).then(resp=>{
            // console.log(resp)
        })
    }

    listarTerceros(){
        this.facturacionMobilsoftService.listar_terceros({}).then(resp=>{
            this.terceros = resp.filas.recordset
        })
    }
    listarDepartamento(){
        this.facturacionMobilsoftService.listar_departamentos({}).then(resp=>{
            this.departamentos = resp.filas.recordsets[0];
            this.departamentos.forEach(element => {
                this.dataForm.ARRAY_DEPARTAMENTO.push({
                    ID: element.coddep,
                    NOMBRE: element.nomdep
                })
            });
            
        })
    }
    listarCiudades(){
        this.facturacionMobilsoftService.listar_ciudades({}).then(resp=>{
            this.ciudades = resp.filas.recordsets[0]
            this.ciudades.forEach(element => {
                this.dataForm.ARRAY_CIUDAD.push({
                    ID: element.coddane.trim(),
                    NOMBRE: element.nommun
                })
            });
            
        }) 
    }
    listarAlmacenes(){
        this.facturacionMobilsoftService.listar_almacenes({}).then(resp => {
            const almacenes = resp.filas.recordsets[0];
            almacenes.forEach(element => {
                this.dataForm.ARRAY_ALMACEN.push({
                    ID: element.codalm,
                    NOMBRE: element.nomalm
                })
            })
        })
    }
    listar_insumos(){
        this.facturacionMobilsoftService.listar_insumos({}).then(resp=>{
          this.dataGridProductos.dataSource = resp.filas.recordset;
        })
    }

    listarCuentas(){
        this.facturacionMobilsoftService.listar_cuentas({TIPO: 3}).then(resp=>{

            const cuentas = resp.filas.recordsets[0];
            cuentas.forEach(element => {
                this.dataForm.ARRAY_CUENTAS.push({
                    ID: element.codcue,
                    NOMBRE: element.nomcba,
                    CODIGO:element.codcue
                })
            })
        })
    }
    cerrar() {
        this.ngxSmartModalService.getModal('facturacion_form_modal').close();
        this.close.emit({ tipo: 1 });
    }

    async consecutivo(){
        //const consecutivo = await this.facturacionClass.buscar_consecutivo().then(result=>result);
        //this.formData.codigo = consecutivo.codigo;
    }

    async guardar(form: FormControl) {
        this.snotifyService.async('Guardando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const data = Object.assign({}, this.formData);
                    //await this.facturacionClass.guardar_almacen(data);
                    form.reset();
                    resolve({
                        title: 'Exito',
                        body: 'Almacen gurdado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                    this.ngxSmartModalService.getModal('facturacion_form_modal').close();
                    this.close.emit({ tipo: 2 });
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo guardar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    atras(){
        this.router.navigate(['admin/facturacion-mobilsoft'])
    }
    verificar_accion(data){

    }
    cancel(){
        
    }

    cambio_tipo_facturacion(event){
        this.TipoFac = (event.target.checked)?1:0
        this.tipFacRap = 1
        this.formData = new FacturacionClass()
    }
    recalcular(){
        this.calcular()
    }

    validarEmail(valor) {
        if (/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(valor)){
           this.valCorreo = true;
        } else {
           this.valCorreo = false;
        }
    }

    modal_usuarios(){
        this.ngxSmartModalService.getModal('myModal').open()
    }

    modal_productos(){
        this.ngxSmartModalService.getModal('productos').open()
    }

    selectionChangedHandler(event) {
        const data = event.data
        this.formData.ID_CLIENTE = data.id;
        this.formData.IDENTIFICACION_CLIENTE = data.codter;
        this.cupoUtilizado(data.codter)
        this.formData.NOMBRES_CLIENTE = data.nomter;
        this.formData.DIRECCION = data.dirter;
        this.formData.TELEFONO = data.telter;
        this.formData.CELULAR = data.celter;
        this.formData.CIUDAD = (data.coddane.trim() == '')? '08001':data.coddane;
        this.formData.CORREO = data.email;
        this.cupo_credito.AUTORIZADO = data.cupo_credito;
        this.cupo_credito.DISPONIBLE = this.cupo_credito.AUTORIZADO - this.cupo_credito.UTILIZADO
        this.formData.PLAZO = data.plazo;
        this.formData.DEPARTAMENTO = (this.ciudades.filter(item => item.coddane.trim() == data.coddane.trim()).length > 0)?this.ciudades.filter(item => item.coddane.trim() == data.coddane.trim())[0].coddep: null
        this.ngxSmartModalService.getModal('myModal').close()
        this.calcular_fecha()
    }
    cupoUtilizado(data){
        this.facturacionMobilsoftService.cupo_utilizado({IDENTIFICACION_CLIENTE: data}).then(resp=>{
            if(resp.filas.recordset[0].UTILIZADO){
                this.cupo_credito.UTILIZADO = resp.filas.recordset[0].UTILIZADO
            }
        })
    }

    selectionChangedHandler2(event) {
        const data = event.data
        if(this.products.filter(item => item.codins == data.codins).length > 0){
            this.snotifyService.warning('Este producto ya ha sido agregado a la lista.', 'Atención')
        }else{
            this.products.push(data)
        }
    }
    quitar(i){
        this.servicios.splice(i, 1)
        this.recalcular()
    }
    editar(i){
        this.dataTable = this.servicios[i]
        this.servicios.splice(i, 1)
        this.recalcular()
    }
    calcular_fecha(){
        const d = new Date()
        const nuevaFecha =  new Date(d.setDate(d.getDate() + (this.formData.PLAZO-1)));
        this.formData.FECHA_VENCIMIENTO = nuevaFecha.getFullYear()+'-'+[nuevaFecha.getMonth()+1]+'-'+nuevaFecha.getDate()
    }
    eliminarProducto(data){

    }
    calcular(){
        let total_desc = 0;
        let total_car = 0;
        let subtotal = 0;
        let total_iva = 0;
        let total = 0;
        let valor_1 = 0;
        let valor_2 = 0;
        let valor_retefuente = 0;
        let valor_reteica = 0;
        let valor_reteiva = 0;
        let fletes = this.facturacion.FLETES;
        let ajuste_peso = 0;
        for (var i = 0; i < this.servicios.length; i++) {
            var element = this.servicios[i];
            if (this.TipoFac) { //FACTURACION RAPIDA
                // if (element.canform > this.ver_mis_existencia(element.codins)) {
                //     element.canform = this.ver_mis_existencia(element.codins)
                //     $.notify({
                //         icon: 'fa fa-exclamation',
                //         title: "<strong>No hay mas inventario para este producto</strong>",
                //         message: ""
                //     }, {
                //         type: 'warning'
                //     });
                // }
                /* var spo = this.listado_precios.map( (producto)  => {
                    return producto.codins
                }).indexOf(element.codins)
                if (pos != -1) {
                    switch (this.facturacion.tippag) {
                        case 1:
                            element.valneg = this.listado_precios[spo].precio_full_ai
                            break;
                        case 2:
                            element.valneg = this.listado_precios[spo].precio_credito_20_ai
                            break;
                        default:
                            element.valneg = this.listado_precios[spo].precio_credito_30_ai
                            break;
                    }
                } */
            }

            if (element.desund > 0) {
                element.carund = 0;
                this.servicios[i].carund = 0;
            } else {
                element.desund = 0
                this.servicios[i].desund = 0;
            }

            if (this.servicios[i].carund == null) {
                element.carund = 0
                this.servicios[i].carund = 0;
            }


            // var vali_carg = Math.round(element.carund / ((100 + 16) / 100));
            // var vali_desc = Math.round(element.desund / ((100 + 16) / 100));
            var val = this.servicios[i].VALOR
            var valnet = this.servicios[i].VALOR

            // total_car += (vali_carg * element.canform)

            subtotal += Math.round(val)
            total_iva += Math.round((valnet * (this.servicios[i].IVA / 100)))
            total += (valnet + Math.round(valnet * (this.servicios[i].IVA / 100)))
            

            if (this.TipoFac) {
                this.servicios[i].valins_lista = (valnet + Math.round(valnet * (this.servicios[i].IVA / 100)));
                this.servicios[i].valins_negociado = (valnet + Math.round(valnet * (this.servicios[i].IVA / 100)));
            }
        }
        if (this.tipFacRap == 2 && this.TipoFac == 1)
            total = total - valor_retefuente - valor_reteica - valor_reteiva + fletes + ajuste_peso
        else total = total - valor_retefuente - valor_reteica - valor_reteiva + fletes + ajuste_peso
        if(this.formData.MODALIDAD_PAGO == 'CR'){
            if(subtotal > this.cupo_credito.DISPONIBLE){
                this.snotifyService.error('Lo sentimos este valor supera tu credito disponible', 'Lo sentimos')
                this.servicios.splice(this.servicios.length-1,1)
            }
        }

        this.facturacion.SUBTOTAL = subtotal;
        this.facturacion.DESCUENTOS = total_desc;
        this.facturacion.CARGOS = total_car;
        this.facturacion.IVA = total_iva;
        this.facturacion.RETEFUENTE = valor_retefuente;
        this.facturacion.RETEICA = valor_reteica;
        this.facturacion.RETEIVA = valor_reteiva;
        this.facturacion.TOTAL = total;
    }
    async seleccion_almacen(){
        this.formData.CODIGO_ALMACEN = this.formData.ALMACEN.toString();
        if(this.formData.CODIGO_ALMACEN != 'null'){
            const res = await this.facturacionMobilsoftService.consecutivo_factura({almacen: this.formData.CODIGO_ALMACEN}).then(res => res)
            this.formData.CONSECUTIVO = res.filas.recordset[0].consecutivo
        }
        this.formData.FECHA = new Date().getDate()+'/'+[new Date().getMonth()+1]+'/'+new Date().getFullYear();

    }
    ver_mis_existencia(i) {
        if(isNaN(parseInt(this.products[i].canform))){
            this.products[i].canform = 0;
            this.recalcular()
        }else{
            if(parseInt(this.products[i].caninv) > parseInt(this.products[i].canform)){
                this.recalcular()
            }else{
                this.products[i].canform = this.products[i].caninv
                this.recalcular()
            }
        }
        
    }
    async facturar(form: FormControl){
        this.snotifyService.async('Guardando factura', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    const resp = await this.facturacionMobilsoftService.guardar_factura({formData: this.formData, dataTable:this.dataTable, servicios: this.servicios, facturacion: this.facturacion}).then(resp=>resp)
                    const doc = await this.facturacionMobilsoftService.reportePDFFactura({id_factura: resp.filas.recordset[0].id_factura})
                    openPdfMake(doc);
                    form.reset();
                    this.servicios = [];
                    this.formData = new FacturacionClass();
                    this.facturacion = new FacturacionTotalesClass();
                    
                    resolve({
                        title: 'Exito',
                        body: 'Facturado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 5000
                        }
                    })
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se pudo facturar  ',
                        config: { closeOnClick: true }
                    })
                }  
            })
        )
        
        
    }
    seleccionar_servicio(event){
        const ARRAY = this.dataForm.ARRAY_SERVICIOS.filter(item=>item.ID == event.target.value);
        if(ARRAY.length > 0){
            this.dataTable.DESCRIPCION = ARRAY[0].DESCRIPCION;
            this.dataTable.UNIDAD = ARRAY[0].UNIDAD;
            this.dataTable.CANTIDAD_UNIDAD = ARRAY[0].CANTIDAD_UNIDAD;
            this.dataTable.IVA = ARRAY[0].IVA;  
            this.dataTable.VALOR = ARRAY[0].VALOR; 
            this.dataTable.SUBTOTAL = ARRAY[0].VALOR; 
            this.dataTable.TOTAL = ARRAY[0].VALOR+((ARRAY[0].IVA/100)*ARRAY[0].VALOR)
        }else{
            this.dataTable = new FacturacionTableClass();
        }
    }
    agregar_concepto(){
        this.dataTable.NOMBRE = this.dataForm.ARRAY_SERVICIOS.filter(item=>item.ID == this.dataTable.SERVICIO)[0].NOMBRE;
        this.dataTable.NOMBRE_UNIDAD = this.dataForm.ARRAY_UNIDAD_MEDIDA.filter(item=>item.ID == this.dataTable.UNIDAD)[0].NOMBRE
        this.servicios.push(this.dataTable)
        this.dataTable = new FacturacionTableClass();
        this.recalcular();
    }

}