import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { APP } from '@app/constant';
import { PdfMakeService, dataPdfMake, dataLogPdf } from '@app/core/pdfmake.config';
import { CurrencyPipe } from '@angular/common';
const line_layout = {
    hLineWidth: function (i, node) {
        return (i === 0 || i === node.table.body.length) ? 0.5 : 0.5;
    },
    vLineWidth: function (i, node) {
        return (i === 0 || i === node.table.widths.length) ? 0.5 : 0.5;
    },
    hLineColor: function (i, node) {
        return (i === 0 || i === node.table.body.length) ? 'black' : 'black';
    },
    vLineColor: function (i, node) {
        return (i === 0 || i === node.table.widths.length) ? 'black' : 'black';
    },
}
@Injectable({
    providedIn: 'root'
})
export class FacturacionMobilsoftService {
    apiEndpoint = APP.ApiEndpoint;
    AppBaseUrl = APP.AppBaseUrl;
    constructor(
        private http: HttpClient,
        private pdfMakeService: PdfMakeService,
        private currencyPipe: CurrencyPipe
    ) { 
        
    }

    async listar_terceros(data: any): Promise<any> {
        const url = `${this.apiEndpoint}/api/terceros/listar`;
        return await this.http.get<any>(url).toPromise();
    }
    async listar_departamentos(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/departamentos/listar`;
        return await this.http.get<any>(url).toPromise();
    }
    async listar_ciudades(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/ciudades/listar`;
        return await this.http.get<any>(url).toPromise();
    }

    async listar_almacenes(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/almacenes/listar`;
        return await this.http.get<any>(url).toPromise();
    }

    async listar_insumos(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/insumos/`;
        return await this.http.post<any>(url, data).toPromise();
    }

    async lista_precios(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/listado_precios/`;
        return await this.http.post<any>(url, data).toPromise();
    }
    
    async listar_cuentas(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/cuentas/listar`;
        return await this.http.post<any>(url,data).toPromise();
    }

    async listar_servicios(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/servicios/listar`;
        return await this.http.get<any>(url).toPromise();
    }

    async guardar_factura(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/facturacion-mobilsoft/guardar`;
        return await this.http.post<any>(url, data).toPromise();
    }
    async listar_detalle_factura(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/facturacion-mobilsoft/listar_factura`;
        return await this.http.post<any>(url, data).toPromise();
    }
    async listar_facturas(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/facturacion-mobilsoft/listar`;
        return await this.http.get<any>(url).toPromise();
    }
    async consecutivo_factura(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/facturacion-mobilsoft/consecutivo_factura`;
        return await this.http.post<any>(url, data).toPromise();
    }
    async listar_formas_pago(): Promise<any>{
        const url = `${this.apiEndpoint}/api/facturacion-mobilsoft/listar_formas_pago`;
        return await this.http.get<any>(url).toPromise();
    }
    async cupo_utilizado(data: any): Promise<any>{
        const url = `${this.apiEndpoint}/api/facturacion-mobilsoft/cupo_utilizado`;
        return await this.http.post<any>(url, data).toPromise();
    }

    async reportePDFFactura(datos: any): Promise<any> {
        const that = this;
        return new Promise(async (resolve, reject) => {
            that.pdfMakeService.convertImgToBase64URL('./assets/img/pos/logo.png', async function (base64Img) {
                            let formData;
                            let servicios = [];
                            let data_impri: any = [
                                [ 
                                    {
                                        text: 'Producto',
                                        style: 'itemsHeader'
                                    }, 
                                    {
                                        text: 'Valor',
                                        style: [ 'itemsHeader', 'center']
                                    }, 
                                    {
                                        text: 'IVA',
                                        style: [ 'itemsHeader', 'center']
                                    }, 
                                    {
                                        text: 'Desc.',
                                        style: [ 'itemsHeader', 'center']
                                    }, 
                                    {
                                        text: 'Total',
                                        style: [ 'itemsHeader', 'center']
                                    } 
                                ]
                            ];

                            const res = await that.listar_detalle_factura(datos).then(result => result);
                            formData = res.filas.recordsets[0][0]
                            for (let i = 0; i < res.filas.recordsets[2].length; i++) {
                                const element = res.filas.recordsets[2][i];
                                data_impri.push([
                                    [{
                                        text: element.codins.trim()+'-'+element.nomins,
                                        style:'itemTitle'
                                    },{
                                        text: element.desins,
                                        style:'itemSubTitle'
                                    }], {
                                        text: that.currencyPipe.transform(element.valins, 'USD', true, '1.0-0'),
                                        style:'itemNumber'
                                    },
                                    {
                                        text: element.ivains+'%',
                                        style:'itemNumber'
                                    },
                                    {
                                        text: that.currencyPipe.transform(0, 'USD', true, '1.0-0'),
                                        style:'itemNumber'
                                    }, 
                                    {
                                        text: that.currencyPipe.transform(element.valins+(element.valins*(element.ivains/100)), 'USD', true, '1.0-0'),
                                        style:'itemTotal'
                                    }, 
                                ]);
                            }
                            // for (let i = 0; i < data.dataTable.length; i++) {
                            //     const element = data.dataTable[i];
                                
                            // }
                    
                            dataLogPdf.layout = line_layout;
                            let dd = {


                                header: {
                                 columns: [
                                   { text: 'FACTURA DE VENTA', style: 'documentHeaderLeft' },
                                   { text: 'FACTURA DE VENTA', style: 'documentHeaderCenter' },
                                   { text: 'FACTURA DE VENTA', style: 'documentHeaderRight' }
                                 ]
                               },
                               footer: {
                                 columns: [
                                   { text: 'FACTURA DE VENTA', style: 'documentFooterLeft' },
                                   { text: 'FACTURA DE VENTA', style: 'documentFooterCenter' },
                                   { text: 'FACTURA DE VENTA', style: 'documentFooterRight' }
                                 ]
                               },
                                 content: [
                                     // Header
                                     {
                                         columns: [
                                             {
                                                 image: base64Img,
                                                 width: 150
                                             },
                                                 
                                             [
                                                 {
                                                     text: 'FACTURA DE VENTA', 
                                                     style: 'invoiceTitle',
                                                     width: '*'
                                                 },
                                                 {
                                                   stack: [
                                                        {
                                                            columns: [
                                                                 {
                                                                     text:'No.', 
                                                                     style:'invoiceSubTitle',
                                                                     width: '*'
                                                                     
                                                                 }, 
                                                                 
                                                                 {
                                                                     text:formData.comprobante,
                                                                     style:'invoiceSubValue',
                                                                     width: 100
                                                                     
                                                                 }
                                                                 ]
                                                        },
                                                        {
                                                            columns: [
                                                                {
                                                                    text:'Fecha',
                                                                    style:'invoiceSubTitle',
                                                                    width: '*'
                                                                }, 
                                                                {
                                                                    text:new Date(formData.fecha).getDate()+'/'+[new Date(formData.fecha).getMonth()+1]+'/'+new Date(formData.fecha).getFullYear(),
                                                                    style:'invoiceSubValue',
                                                                    width: 100
                                                                }
                                                                ]
                                                            
                                                        },
                                                        {
                                                            columns: [
                                                                {
                                                                    text:'Forma de pago',
                                                                    style:'invoiceSubTitle',
                                                                    width: '*'
                                                                }, 
                                                                {
                                                                    text: formData.forma_pago,
                                                                    style:'invoiceSubValue',
                                                                    width: 100
                                                                }
                                                            ]
                                                        },{
                                                            columns: [
                                                                {
                                                                    text:'Almacen',
                                                                    style:'invoiceSubTitle',
                                                                    width: '*'
                                                                }, 
                                                                {
                                                                    text: formData.nomalm,
                                                                    style:'invoiceSubValue',
                                                                    width: 100
                                                                }
                                                            ]
                                                        } 
                                                    ]
                                                 }
                                             ],
                                         ],
                                     },
                                     // Billing Headers
                                     {
                                         columns: [
                                             {
                                                 text: 'Empresa',
                                                 style:'invoiceBillingTitle',
                                                 
                                             },
                                             {
                                                 text: 'Cliente',
                                                 style:'invoiceBillingTitle',
                                                 
                                             },
                                         ]
                                     },
                                     // Billing Details
                                     {
                                         columns: [
                                             {
                                                 text: 'Mobilsoft Saas. \n NIT 2034786221-6 \n Tel 32743598 \n mobilsoftsaas@gmail.com',
                                                 style: 'invoiceBillingDetails'
                                             },
                                             {
                                                 text: formData.nomter+' \n CC '+formData.codter+' \n Tel '+ formData.telter+' \n '+formData.email,
                                                 style: 'invoiceBillingDetails'
                                             },
                                         ]
                                     },
                                     // Billing Address Title
                                     {
                                         columns: [
                                             {
                                                 text: 'Dirección',
                                                 style: 'invoiceBillingAddressTitle'
                                             },
                                             {
                                                 text: 'Dirección',
                                                 style: 'invoiceBillingAddressTitle'
                                             },
                                         ]
                                     },
                                     // Billing Address
                                     {
                                         columns: [
                                             {
                                                 text: 'Calle 84 Villa Andalucía Bloq. 8 Apto 201 \n Barranquilla - Atlantico\n   COLOMBIA',
                                                 style: 'invoiceBillingAddress'
                                             },
                                             {
                                                 text: 'CR 23 # 55A - 66 \n Barranquilla - Atlantico\n   COLOMBIA',
                                                 style: 'invoiceBillingAddress'
                                             },
                                         ]
                                     },
                                     // Line breaks
                                     '\n\n',
                                     // Items
                                     {
                                       table: {
                                         // headers are automatically repeated if the table spans over multiple pages
                                         // you can declare how many rows should be treated as headers
                                         headerRows: 1,
                                         widths: [ '*', 'auto', 40, 40, 80 ],
                                 
                                         body: data_impri,
                                       }, // table
                                     //  layout: 'lightHorizontalLines'
                                     },
                                      //NOTA
                                    //  { 
                                    //     text: 'NOTA',
                                    //     style:'notesTitle'
                                    // },
                                    { 
                                        text: (formData.codigo_forma_pago == 'TN')?'Transferir el monto de '+that.currencyPipe.transform(formData.total, 'USD', true, '1.0-0')+' en la cuenta '+formData.nomcue+' No '+formData.codcue+'.':'',
                                        style:'notesText'
                                    },
                                  // TOTAL
                                     {
                                       table: {
                                         // headers are automatically repeated if the table spans over multiple pages
                                         // you can declare how many rows should be treated as headers
                                         headerRows: 0,
                                         widths: [ '*', 90 ],
                                 
                                         body: [
                                           // Total
                                           [ 
                                               {
                                                   text:'SUBTOTAL',
                                                   style:'itemsFooterSubTitle'
                                               }, 
                                               { 
                                                   text: that.currencyPipe.transform(formData.subtotal, 'USD', true, '1.0-0'),
                                                   style:'itemsFooterSubValue'
                                               }
                                           ],
                                           [ 
                                               {
                                                   text:'IVA',
                                                   style:'itemsFooterSubTitle'
                                               },
                                               {    
                                                   text: that.currencyPipe.transform(formData.valor_iva, 'USD', true, '1.0-0'),
                                                   style:'itemsFooterSubValue'
                                               }
                                           ],
                                           [ 
                                                {
                                                    text:'FLETES',
                                                    style:'itemsFooterSubTitle'
                                                },
                                                {
                                                    text: that.currencyPipe.transform(formData.valor_fletes, 'USD', true, '1.0-0'),
                                                    style:'itemsFooterSubValue'
                                                }
                                            ],
                                           [ 
                                               {
                                                   text:'TOTAL',
                                                   style:'itemsFooterTotalTitle'
                                               }, 
                                               {
                                                   text:  that.currencyPipe.transform(formData.total, 'USD', true, '1.0-0'),
                                                   style:'itemsFooterTotalValue'
                                               }
                                           ],
                                         ]
                                       }, // table
                                       layout: 'lightHorizontalLines'
                                     },
                                    
                                     // Signature
                                     {
                                         columns: [
                                             {
                                                 text:'',
                                             },
                                             {
                                                 stack: [
                                                     { 
                                                         text: '_________________________________',
                                                         style:'signaturePlaceholder'
                                                     },
                                                     { 
                                                         text: 'MOBILSOFTSAAS',
                                                         style:'signatureName'
                                                         
                                                     },
                                                     { 
                                                         text: 'ADMINISTRACION',
                                                         style:'signatureJobTitle'
                                                         
                                                     }
                                                     ],
                                                width: 180
                                             },
                                         ]
                                     },
                                    
                                    // { 
                                    //     text: 'DESCRIPCIÓN',
                                    //     style:'notesTitle'
                                    // },
                                    // { 
                                    //     text: (formData.descripcion == '' || formData.descripcion == null)? 'Ninguna.': formData.descripcion,
                                    //     style:'notesText'
                                    // },
                                    
                                 ],
                                 styles: {
                                     // Document Header
                                     documentHeaderLeft: {
                                         fontSize: 10,
                                         margin: [5,5,5,5],
                                         alignment:'left'
                                     },
                                     documentHeaderCenter: {
                                         fontSize: 10,
                                         margin: [5,5,5,5],
                                         alignment:'center'
                                     },
                                     documentHeaderRight: {
                                         fontSize: 10,
                                         margin: [5,5,5,5],
                                         alignment:'right'
                                     },
                                     // Document Footer
                                     documentFooterLeft: {
                                         fontSize: 10,
                                         margin: [5,5,5,5],
                                         alignment:'left'
                                     },
                                     documentFooterCenter: {
                                         fontSize: 10,
                                         margin: [5,5,5,5],
                                         alignment:'center'
                                     },
                                     documentFooterRight: {
                                         fontSize: 10,
                                         margin: [5,5,5,5],
                                         alignment:'right'
                                     },
                                     // Invoice Title
                                     invoiceTitle: {
                                         fontSize: 22,
                                         bold: true,
                                         alignment:'right',
                                         margin:[0,0,0,15]
                                     },
                                     // Invoice Details
                                     invoiceSubTitle: {
                                         fontSize: 12,
                                         alignment:'right',
                                         bold: true
                                     },
                                     invoiceSubValue: {
                                         fontSize: 12,
                                         alignment:'right',
                                     },
                                     // Billing Headers
                                     invoiceBillingTitle: {
                                         fontSize: 14,
                                         bold: true,
                                         alignment:'left',
                                         margin:[0,20,0,5],
                                     },
                                     // Billing Details
                                     invoiceBillingDetails: {
                                         alignment:'left'
                             
                                     },
                                     invoiceBillingAddressTitle: {
                                         margin: [0,7,0,3],
                                         bold: true
                                     },
                                     invoiceBillingAddress: {
                                         
                                     },
                                     // Items Header
                                     itemsHeader: {
                                         margin: [0,5,0,5],
                                         bold: true
                                     },
                                     // Item Title
                                     itemTitle: {
                                         bold: true,
                                     },
                                     itemSubTitle: {
                                         italics: true,
                                         fontSize: 9
                                     },
                                     itemNumber: {
                                         margin: [0,5,0,5],
                                         alignment: 'center',
                                     },
                                     itemTotal: {
                                         margin: [0,5,0,5],
                                         bold: true,
                                         alignment: 'center',
                                     },
                             
                                     // Items Footer (Subtotal, Total, Tax, etc)
                                     itemsFooterSubTitle: {
                                         margin: [0,5,0,5],
                                         bold: true,
                                         alignment:'right',
                                     },
                                     itemsFooterSubValue: {
                                         margin: [0,5,0,5],
                                         bold: true,
                                         alignment:'center',
                                     },
                                     itemsFooterTotalTitle: {
                                         margin: [0,5,0,5],
                                         bold: true,
                                         alignment:'right',
                                     },
                                     itemsFooterTotalValue: {
                                         margin: [0,5,0,5],
                                         bold: true,
                                         alignment:'center',
                                     },
                                     signaturePlaceholder: {
                                         margin: [0,70,0,0],   
                                     },
                                     signatureName: {
                                         bold: true,
                                         alignment:'center',
                                     },
                                     signatureJobTitle: {
                                         italics: true,
                                         fontSize: 10,
                                         alignment:'center',
                                     },
                                     notesTitle: {
                                       fontSize: 10,
                                       bold: true,  
                                       margin: [0,40,0,3],
                                     },
                                     notesText: {
                                       fontSize: 8,
                                       margin: [0,3,0,0]
                                     },
                                     center: {
                                         alignment:'center',
                                     },
                                 },
                                 defaultStyle: {
                                     columnGap: 20,
                                 }
                             }
                            resolve(dd);
            }, null)
        });
    }
}
 