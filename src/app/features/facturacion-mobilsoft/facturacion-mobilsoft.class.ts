export class FacturacionClass {
    constructor(
        public ID?: number,
        public PROVEEDOR?: string,
        public MODALIDAD_PAGO?: string,
        public TIPO_FACTURA?: string,
        public FECHA?: string,
        public ID_CLIENTE?: number,
        public IDENTIFICACION_CLIENTE?: string,
        public DEPARTAMENTO?: number,
        public NOMBRES_CLIENTE?: string,
        public DIRECCION?: string,
        public TELEFONO?: string,
        public CELULAR?: string,
        public ESTADO?: number,
        public VENDEDOR?: string,
        public PLAZO?: number,
        public FECHA_VENCIMIENTO?: string,
        public CIUDAD?: number,
        public ALMACEN?: number,
        public CODIGO_ALMACEN?: string,
        public CORREO?: string,
        public CODIGO_REMISION?: string,
        public DESCRIPCION?: string,
        public CONSECUTIVO?: string,
        public SERVICIO?: string,
        public COMENTARIO?: string,
        public CUENTA_BANCARIA?: string,
        public NOMBRE_CUENTA_BANCARIA?: string,
    ) {
        this.ID = null;
        this.PROVEEDOR = null;
        this.MODALIDAD_PAGO = null;
        this.TIPO_FACTURA = null;
        this.FECHA = new Date().getDate()+'/'+[new Date().getMonth()+1]+'/'+new Date().getFullYear();
        this.ID_CLIENTE = null;
        this.IDENTIFICACION_CLIENTE = null;
        this.DEPARTAMENTO = null;
        this.ESTADO = null;
        this.DIRECCION = null;
        this.TELEFONO = null;
        this.CELULAR = null;
        this.VENDEDOR = null;
        this.CIUDAD = null;
        this.ALMACEN = null;
        this.CORREO = null;
        this.CODIGO_REMISION = null;
        this.DESCRIPCION = null;
        this.CODIGO_ALMACEN = null;
        this.CONSECUTIVO = null;
        this.COMENTARIO = null;
        this.CUENTA_BANCARIA = null;
        this.NOMBRE_CUENTA_BANCARIA = null;
        
    }
}
export class FacturacionTotalesClass {
    constructor(
        public SUBTOTAL?: number,
        public DESCUENTOS?: number,
        public CARGOS?: number,
        public IVA?: number,
        public RETEFUENTE?: number,
        public RETEICA?: number,
        public RETEIVA?: number,
        public FLETES?: number,
        public TOTAL?: number
    ) {
        this.SUBTOTAL= 0;
        this.DESCUENTOS= 0;
        this.CARGOS= 0;
        this.IVA= 0;
        this.RETEFUENTE= 0;
        this.RETEICA= 0;
        this.RETEIVA= 0;
        this.FLETES= 0;
        this.TOTAL= 0;
    }
}
export class FacturacionCreditoClass {
    constructor(
        public AUTORIZADO?: number,
        public UTILIZADO?: number,
        public DISPONIBLE?: number,
        public PENDIENTE?: number
    ) {
        this.AUTORIZADO = 0;
        this.UTILIZADO = 0;
        this.DISPONIBLE = 0;
        this.PENDIENTE = 0;
    }
}
export class FacturacionArraysClass {
    constructor(
        public ARRAY_PROVEEDORES?: Array<any>,
        public ARRAY_MODALIDAD_PAGO?: Array<any>,
        public ARRAY_TIPO_FACTURA?: Array<any>,
        public ARRAY_DEPARTAMENTO?: Array<any>,
        public ARRAY_CIUDAD?: Array<any>,
        public ARRAY_VENDEDOR?: Array<any>,
        public ARRAY_ALMACEN?: Array<any>,
        public ARRAY_SERVICIOS?: Array<any>,
        public ARRAY_UNIDAD_MEDIDA?: Array<any>,
        public ARRAY_CUENTAS?: Array<any>
    ) {
        this.ARRAY_PROVEEDORES = [{
            ID: null,
            NOMBRE: 'Seleccione el proveedor'
        }]
        this.ARRAY_MODALIDAD_PAGO = [{
            ID: null,
            NOMBRE: 'Seleccione la modalidad de pago'
        }]
        this.ARRAY_TIPO_FACTURA = [{
            ID: null,
            NOMBRE: 'Seleccione tipo de factura'
        },
        {
            ID: 1,
            NOMBRE: 'Facturación POS'
        },
        {
            ID: 2,
            NOMBRE: 'Facturación electronica'
        },
        {
            ID:3,
            NOMBRE: 'Facturación por computador'
        },
        {
            ID: 4,
            NOMBRE: 'Facturación imprenta'
        }]
        this.ARRAY_DEPARTAMENTO = [{
            ID: null,
            NOMBRE: 'Seleccione el departamento'
        }]
        this.ARRAY_VENDEDOR = [{
            ID: null,
            NOMBRE: 'Seleccione vendedor'
        }]
        this.ARRAY_ALMACEN = [{
            ID: null,
            NOMBRE: 'Seleccione un almacen'
        }]
        this.ARRAY_CIUDAD = [{
            ID: null,
            NOMBRE: 'Seleccione la ciudad'
        }]
        this.ARRAY_SERVICIOS = [{
            ID: null,
            NOMBRE: 'Seleccione un servicio',
            DESCRIPCION: null,
            IVA: null,
            VALOR: null,
            UNIDAD: null,
            CANTIDAD_UNIDAD: null
        }]
        this.ARRAY_UNIDAD_MEDIDA = [{
            ID: null,
            NOMBRE: 'Seleccione Unidad'
        },
        {
            ID: 1,
            NOMBRE: 'Dias'
        },
        {
            ID: 2,
            NOMBRE: 'Meses'
        },{
            ID: 3,
            NOMBRE: 'Semestres'
        }]

        this.ARRAY_CUENTAS = [{
            ID: null,
            NOMBRE: 'Seleccione una cuenta',
            CODIGO: ''
        }]
    }
    
}
export class FacturacionTableClass{
        constructor(
            public SERVICIO?: string,
            public UNIDAD?: number,
            public CANTIDAD_UNIDAD?: number,
            public CANTIDAD?: number,
            public NOMBRE_UNIDAD?: string,
            public NOMBRE?: string,
            public DESCRIPCION?: string,
            public VALOR?: number,
            public IVA?: number,
            public SUBTOTAL?: number,
            public TOTAL?: number,

        ){
            this.SERVICIO = null;
            this.NOMBRE = null;
            this.VALOR = null;
            this.IVA = null;
            this.SUBTOTAL = null;
            this.TOTAL = null;
            this.DESCRIPCION = null;
            this.UNIDAD = null;
            this.CANTIDAD = null;
            this.CANTIDAD_UNIDAD = null;
            this.NOMBRE_UNIDAD = null;
        }
}