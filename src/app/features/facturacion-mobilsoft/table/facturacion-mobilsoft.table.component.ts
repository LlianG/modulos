import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { NotificationService } from '@app/core/services';
import { FacturacionClass } from '../facturacion-mobilsoft.class';
import { FacturacionMobilsoftService } from '../facturacion-mobilsoft.service';
import { Subject } from 'rxjs';
import { SnotifyService } from 'ng-snotify';
import { DxDataGridComponent } from 'devextreme-angular';
import { openPdfMake } from '@app/core/pdfmake.config';

@Component({
    selector: 'app-facturacion-mobilsoft-table',
    templateUrl: 'facturacion-mobilsoft.table.component.html',
    styleUrls: ['facturacion-mobilsoft.table.component.css']
})

export class FacturacionMobilsoftTableComponent implements OnInit {
    @ViewChild('gridFacturas') dataGridFacturas: DxDataGridComponent;
    stateFilter: number;
    listadoFacturaciones: FacturacionClass[];
    facturas: any;
    @Input() set state(state: number) {
        this.stateFilter = state;
        this.listar_datos();
    }
    @Input() set cambio(cambio: number) {
        if (cambio > 1) {
            this.listar_datos();
        }
    }
    @Output() close = new EventEmitter();

    constructor(
        private facturacionMobilsoftService: FacturacionMobilsoftService,
        private snotifyService: SnotifyService
    ) {
    }

    ngOnInit() { 
        this.listarFacturas()
    }


    async listar_datos() {
        // this.listadoAlmacenes = await this.almacenService.listar_almacenes({ estado: this.stateFilter }).then(result => result);
        // this.rerenderTable();
    }
    async editar(id: number) {
        this.close.emit({ tipo: 2, id: id });
    }
    async listarFacturas(){
        this.facturacionMobilsoftService.listar_facturas({}).then(res=>{
            this.facturas = res.filas.recordset
        })
    }
    async desactivar(id: number) {
        this.snotifyService.async('Actualizando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    // await this.almacenService.cambiar_estado(id, 0);
                    this.close.emit({ tipo: 3 });
                    resolve({
                        title: 'Exito',
                        body: 'Almacen desactivado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo desactivar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    async reactivar(id: number) {
        this.snotifyService.async('Actualizando almacen', 'Procesando',
            new Promise(async (resolve, reject) => {
                try {
                    //await this.almacenService.cambiar_estado(id, 1);
                    this.close.emit({ tipo: 4 });
                    resolve({
                        title: 'Exito',
                        body: 'Almacen reactivado correctamente',
                        config: {
                            showProgressBar: true,
                            closeOnClick: true,
                            timeout: 3000
                        }
                    })
                } catch (error) {
                    reject({
                        title: 'Error!!!',
                        body: 'No se puedo reactivar la almacen',
                        config: { closeOnClick: true }
                    })
                }
            })
        );
    }
    async selectionChangedHandler(event){
        const doc = await this.facturacionMobilsoftService.reportePDFFactura({id_factura: event.data.id})
        openPdfMake(doc);
    }
}