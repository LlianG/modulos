import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NotasBancariasService } from './notas-bancarias.service';
import { Router } from '@angular/router'

@Component({
    selector: 'app-notas-bancarias',
    templateUrl: './notas-bancarias.component.html',
    styleUrls: ['./notas-bancarias.component.css']
})
export class NotasBancariasComponent implements OnInit {

    c_id_table: number;
    c_accion_form: number;
    c_cambio_table: number;
    f_state_table: number;

    constructor(
        private notasBancariasService: NotasBancariasService,
        private titleService: Title,
        private route: Router
    ) {
        this.titleService.setTitle('Notas bancarias');
        this.c_accion_form = 0;
        this.c_cambio_table = 1;
        this.f_state_table = 1;
    }

    ngOnInit() {
    }

    newNotasBancarias() {
        this.route.navigate(['admin/notas-bancarias/new'])
    }
    event_table_notas_bancarias(event: any): void {
        switch (event.tipo) {
            case 1: //:: Nuevo
                this.c_accion_form = event.tipo;
                break;
            case 2: //:: Editar
                this.c_accion_form = event.tipo;
                this.c_id_table = event.id;
                break;
            case 3: //:: Desactivado
                this.c_cambio_table++;
                break;
            case 4: //:: Reactivado
                this.c_cambio_table++;
                break;
        }
    }

    event_form_notas_bancarias(event): void {
        switch (event.tipo) {
            case 1: //:: Cerró

                break;
            case 2: //:: Guardó
                this.c_cambio_table++;
                break;
            case 3: //:: Editó
                this.c_cambio_table++;
                break;
        }
        this.c_accion_form = 0;
        this.c_id_table = null;
    }

}
