export class PedidoClass {
    constructor(
        public ID?: number,
        public NIT_EMPRESA?: string,
        public RAZON_SOCIAL?: string,
        public FECHA?: Date,
        public NUMERO_COMPROBANTE?: string,
        public CONSIGNAR_CUENTA?: string,
        public ALMACEN?: number,
        public IDENTIFICACION_CLIENTE?: string,
        public NOMBRES_CLIENTE?: string,
        public TELEFONO?: number,
        public DIRECCION?: string,
        public CIUDAD?:number,  
        public DEPARTAMENTO?: number,
        public COBRADOR?: string,
        public VENDEDOR?: string,
        public MODALIDAD_PAGO?: number,
        public NOMBRE_COBRADOR?: string,
        public ABONO_FACTURA?: number,
        public TOTAL_DEUDAS?: number,
        public DESCRIPCION?: string
    ) {
        this.ID = null;
        this.NIT_EMPRESA = null;
        this.RAZON_SOCIAL = null;
        this.FECHA = new Date();
        this.NUMERO_COMPROBANTE = null;
        this.CONSIGNAR_CUENTA = null;
        this.ALMACEN = null;
        this.IDENTIFICACION_CLIENTE = null;
        this.NOMBRES_CLIENTE = null;
        this.DEPARTAMENTO = null;
        this.COBRADOR = null;
        this.NOMBRE_COBRADOR = null;
        this.ABONO_FACTURA = 0;
        this.TOTAL_DEUDAS = 0;
        this.DESCRIPCION = null;
        this.VENDEDOR = null;
        this.MODALIDAD_PAGO = null
    }
}

export class PedidoTotalesClass {
    constructor(
        public ABONO_FACTURA?: number,
        public DESCUENTOS_PRONTO_PAGO?: number,
        public OTROS_DESCUENTO?: number,
        public OTRAS_RETENCIONES?: number,
        public OTROS_CARGOS?: number,
        public TOTAL_PAGAR?: number,
        public FORMAS_PAGO?: number,
        public DIFERENCIA?: number,
        public SALDO_FAVOR?: number,
        public NUEVO_SALDO?: number
    ) {
        this.ABONO_FACTURA = 0;
        this.DESCUENTOS_PRONTO_PAGO = 0;
        this.OTROS_DESCUENTO = 0;
        this.OTRAS_RETENCIONES = 0;
        this.OTROS_CARGOS = 0;
        this.TOTAL_PAGAR = 0;
        this.FORMAS_PAGO = 0;
        this.DIFERENCIA = 0;
        this.SALDO_FAVOR  = 0;
        this.NUEVO_SALDO = 0;
    }
}

export class PedidosCuposCreditoClass {
    constructor(
        public AUTORIZADO?: number,
        public UTILIZADO?: number,
        public PENDIENTE?: number,
        public DISPONIBLE?: number,
    ){
        this.AUTORIZADO = 0;
        this.UTILIZADO = 0;
        this.PENDIENTE = 0;
        this.DISPONIBLE = 0;
    }
}

export class PedidoTerceroScore{
    constructor(
        public CLASIFICACION?: number,
        public TOTAL_PUNTAJE?: number,
    ){
        this.CLASIFICACION = 0
        this.TOTAL_PUNTAJE = 0
    }
}

export class PedidoValores{
    constructor(
        public SUBTOTAL1?: number,
        public SUBTOTAL2?: number,
        public SUBTOTAL3?: number,
        public DESCUENTO1?: number,
        public DESCUENTO2?: number,
        public DESCUENTO3?: number,
        public CARGOS1?: number,
        public CARGOS2?: number,
        public CARGOS3?: number,
        public IVA1?: number,
        public IVA2?: number,
        public IVA3?: number,
        public TOTAL1?: number,
        public TOTAL2?: number,
        public TOTAL3?: number,
    ){
        this.SUBTOTAL1 = 0;
        this.SUBTOTAL2 = 0;
        this.SUBTOTAL3 = 0;
        this.DESCUENTO1 = 0;
        this.DESCUENTO2 = 0;
        this.DESCUENTO3 = 0;
        this.CARGOS1 = 0;
        this.CARGOS2 = 0;
        this.CARGOS3 = 0;
        this.IVA1 = 0;
        this.IVA2 = 0;
        this.IVA3 = 0;
        this.TOTAL1 = 0;
        this.TOTAL2 = 0;
        this.TOTAL3 = 0;
    }
}

export class PedidosnArraysClass {
    constructor(
        public ARRAY_ALMACEN?: Array<any>,
        public ARRAY_CUENTAS?: Array<any>,
        public ARRAY_MODALIDAD_PAGO?: Array<any>,
        public ARRAY_VENDEDOR?: Array<any>
    ) {
        this.ARRAY_ALMACEN = [{
            ID: null,
            NOMBRE: 'Seleccione un almacen'
        }]
        this.ARRAY_CUENTAS = [{
            ID: null,
            NOMBRE: 'Seleccione una cuenta'
        }]
        this.ARRAY_MODALIDAD_PAGO = [{
            ID: null,
            NOMBRE: 'Seleccione una modalidad'
        },
        {
            ID: 1,
            NOMBRE: 'CONTADO 1 - 10 DIAS'
        },
        {
            ID: 2,
            NOMBRE: 'CONTADO 11 - 20 DIAS'
        },
        {
            ID: 3,
            NOMBRE: 'CONTADO 21 - 30 DIAS'
        }]
        this.ARRAY_VENDEDOR =  [{
            ID: null,
            NOMBRE: 'Seleccione un vendedor'
        }]
    }
}