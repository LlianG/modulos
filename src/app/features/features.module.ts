import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { featureRouting } from './features.routing';

import {HomeComponent} from "./home/home.component";
import { SharedModule } from '@app/shared/shared.module';


/***********************************\
 * IMPORTACION DE MODULOS *
\************************************/

import { BaseComponent } from './base/base.component';
import { BaseService } from './base/base.service';
import { BaseFormComponent } from './base/form/base.form.component';
import { BaseTableComponent } from './base/table/base.table.component';
import { FacturacionComponent } from './facturacion/facturacion.component';
import { FacturacionFormComponent } from './facturacion/form/facturacion.form.component';
import { FacturacionTableComponent } from './facturacion/table/facturacion.table.component';
import { FacturacionService } from './facturacion/facturacion.service';
import { ReciboDeCajaComponent } from './recibo-caja/recibo-caja.component';
import { ReciboDeCajaTableComponent } from './recibo-caja/table/recibo-caja.table.component';
import { ReciboDeCajaFormComponent } from './recibo-caja/form/recibo-caja.form.component';
import { ReciboDeCajaService } from './recibo-caja/recibo-caja.service';
import { PedidosComponent } from './pedidos/pedidos.component';
import { PedidosFormComponent } from './pedidos/form/pedidos.form.component';
import { PedidosTableComponent } from './pedidos/table/pedidos.table.component';
import { PedidosService } from './pedidos/pedidos.service';
import { RequisicionComponent } from './requisicion/requisicion.component';
import { RequisicionFormComponent } from './requisicion/form/requisicion.form.component';
import { RequisicionTableComponent } from './requisicion/table/requisicion.table.component';
import { RequisicionService } from './requisicion/requisicion.service';
import { NotasBancariasComponent } from './notas-bancarias/notas-bancarias.component';
import { NotasBancariasFormComponent } from './notas-bancarias/form/notas-bancarias.form.component';
import { NotasBancariasTableComponent } from './notas-bancarias/table/notas-bancarias.table.component';
import { NotasBancariasService } from './notas-bancarias/notas-bancarias.service';
import { ConsignacionesComponent } from './consignaciones/consignaciones.component';
import { ConsignacionesFormComponent } from './consignaciones/form/consignaciones.form.component';
import { ConsignacionesTableComponent } from './consignaciones/table/consignaciones.table.component';
import { ConsignacionesService } from './consignaciones/consignaciones.service';
import { NotasProveedorComponent } from './notas-proveedor/notas-proveedor.component';
import { NotasProveedorFormComponent } from './notas-proveedor/form/notas-proveedor.form.component';
import { NotasProveedorTableComponent } from './notas-proveedor/table/notas-proveedor.table.component';
import { NotasProveedorService } from './notas-proveedor/notas-proveedor.service';
import { InventarioAlmacenComponent } from './inventario-almacen/inventario-almacen.component';
import { InventarioAlmacenFormComponent } from './inventario-almacen/form/inventario-almacen.form.component';
import { InventarioAlmacenTableComponent } from './inventario-almacen/table/inventario-almacen.table.component';
import { InventarioAlmacenService } from './inventario-almacen/inventario-almacen.service';
import { EgresosComponent } from './egresos/egresos.component';
import { EgresosFormComponent } from './egresos/form/egresos.form.component';
import { EgresosTableComponent } from './egresos/table/egresos.table.component';
import { EgresosService } from './egresos/egresos.service';
import { NotasClientesComponent } from './notas-clientes/notas-clientes.component';
import { NotasClientesFormComponent } from './notas-clientes/form/notas-clientes.form.component';
import { NotasClientesTableComponent } from './notas-clientes/table/notas-clientes.table.component';
import { NotasClientesService } from './notas-clientes/notas-clientes.service';
import { RadicacionFacturaComponent } from './radicacion-factura/radicacion-factura.component';
import { RadicacionFacturaFormComponent } from './radicacion-factura/form/radicacion-factura.form.component';
import { RadicacionFacturaTableComponent } from './radicacion-factura/table/radicacion-factura.table.component';
import { RadicacionFacturaService } from './radicacion-factura/radicacion-factura.service';
import { DevolucionesClientesComponent } from './devoluciones-clientes/devoluciones-clientes.component';
import { DevolucionesClientesFormComponent } from './devoluciones-clientes/form/devoluciones-clientes.form.component';
import { DevolucionesClientesTableComponent } from './devoluciones-clientes/table/devoluciones-clientes.table.component';
import { DevolucionesClientesService } from './devoluciones-clientes/devoluciones-clientes.service';
import { FacturacionMobilsoftComponent } from './facturacion-mobilsoft/facturacion-mobilsoft.component';
import { FacturacionMobilsoftTableComponent } from './facturacion-mobilsoft/table/facturacion-mobilsoft.table.component';
import { FacturacionMobilsoftFormComponent } from './facturacion-mobilsoft/form/facturacion-mobilsoft.form.component';
import { FacturacionMobilsoftService } from './facturacion-mobilsoft/facturacion-mobilsoft.service';
import { PdfMakeService } from '@app/core/pdfmake.config';



@NgModule({
  imports: [
    CommonModule,
    featureRouting,
    SharedModule
  ],
  declarations: [
    HomeComponent,
    BaseComponent,
    BaseFormComponent,
    BaseTableComponent,

    FacturacionComponent,    
    FacturacionFormComponent,
    FacturacionTableComponent,

    ReciboDeCajaComponent,
    ReciboDeCajaTableComponent,
    ReciboDeCajaFormComponent,

    PedidosComponent,
    PedidosFormComponent,
    PedidosTableComponent,

    RequisicionComponent,
    RequisicionFormComponent,
    RequisicionTableComponent,

    NotasBancariasComponent,
    NotasBancariasFormComponent,
    NotasBancariasTableComponent,

    ConsignacionesComponent,
    ConsignacionesFormComponent,
    ConsignacionesTableComponent,

    NotasProveedorComponent,
    NotasProveedorFormComponent,
    NotasProveedorTableComponent,

    InventarioAlmacenComponent,
    InventarioAlmacenFormComponent,
    InventarioAlmacenTableComponent,

    EgresosComponent,
    EgresosFormComponent,
    EgresosTableComponent,
    
    NotasClientesComponent,
    NotasClientesFormComponent,
    NotasClientesTableComponent,
    
    RadicacionFacturaComponent,
    RadicacionFacturaFormComponent,
    RadicacionFacturaTableComponent,

    DevolucionesClientesComponent,
    DevolucionesClientesFormComponent,
    DevolucionesClientesTableComponent,
    FacturacionMobilsoftComponent,
    FacturacionMobilsoftTableComponent,
    FacturacionMobilsoftFormComponent,



  ],
  providers:[
    BaseService,
    
    FacturacionService,

    ReciboDeCajaService,

    PedidosService,

    RequisicionService,

    NotasBancariasService,

    ConsignacionesService,

    NotasProveedorService,

    InventarioAlmacenService, 

    EgresosService,
    
    NotasClientesService,
    
    RadicacionFacturaService,

    DevolucionesClientesService,
    
    FacturacionMobilsoftService,
    
    PdfMakeService,

    

  ]
})
export class FeaturesModule { }
