export class BaseClass {
    constructor(
        public id?: number,
        public codigo?: string,
        public nombre?: string,
        public telefono?: string,
        public direccion?: string,
        public estado?: number,
    ) {
        this.id = null;
        this.codigo = null;
        this.nombre = null;
        this.telefono = null;
        this.direccion = null;
        this.estado = null;
    }
}