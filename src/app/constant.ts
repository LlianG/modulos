import { environment } from "@env/environment";

export const APP: any = {
    ApiEndpoint: environment.urlMaster,
    AppBaseUrl: environment.urlApp,
}