import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { NgxSmartModalModule } from 'ngx-smart-modal';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { SnotifyModule, ToastDefaults, SnotifyService } from "ng-snotify";
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';



export const ToastConfig = Object.assign({}, ToastDefaults);
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
    CoreModule,
    SnotifyModule,
    FontAwesomeModule,

    NgxSmartModalModule.forRoot()
  ],
  providers: [
    { provide: 'SnotifyToastConfig', useValue: ToastConfig},
    SnotifyService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
